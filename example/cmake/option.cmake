#############################################################################################
## Service version
############################################################################################
set(SERVICE_VERSION_MAJOR 0)
set(SERVICE_VERSION_MINOR 4)
set(SERVICE_VERSION_PATCH 0)

if(NOT DEFINED BUILD_VERSION)
    set(SERVICE_VERSION_BUILD 0)
else()
    set(SERVICE_VERSION_BUILD ${BUILD_VERSION})
endif()

string(TIMESTAMP SERVICE_BUILD_TIME "%Y-%m-%d %H:%M:%S")
set(SERVICE_VERSION V${SERVICE_VERSION_MAJOR}.${SERVICE_VERSION_MINOR}.${SERVICE_VERSION_PATCH}.${SERVICE_VERSION_BUILD})
message(STATUS "${SERVICE_NAME} time: ${SERVICE_BUILD_TIME}")
message(STATUS "${SERVICE_NAME} version: ${SERVICE_VERSION}")

#############################################################################################
## include/library directories, link library
#############################################################################################
include_directories(${CMAKE_BINARY_DIR})
link_directories(${PROJECT_SOURCE_DIR}/../lib)
include_directories(${PROJECT_SOURCE_DIR}/../include)

#############################################################################################
## compile options : https://cmake.org/cmake/help/v3.10/command/add_compile_options.html
#############################################################################################

set(CMAKE_CXX_STANDARD 17)
if(CMAKE_COMPILER_IS_GNUCC)
    add_compile_options(-Wall -Werror -fno-strict-aliasing)
endif(CMAKE_COMPILER_IS_GNUCC)

#############################################################################################
## configration file
#############################################################################################
set(IMAGE_TAG $ENV{IMAGE_TAG})
set(IMAGE_NAME $ENV{IMAGE_NAME})
configure_file(
    ${PROJECT_SOURCE_DIR}/cmake/config.h.in ${CMAKE_BINARY_DIR}/config.h
)
