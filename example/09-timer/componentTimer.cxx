#include "erpc/erpc.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"

class componentTimer: public simpost::Component
{
public:
    explicit componentTimer() : Component("moduleTimer", "V0.1.0") {}
    virtual ~componentTimer() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void timerHandler(erpc::erpc_timer_t handle, void *arg);
private:
    erpc::erpc_timer_t m_timer_ctx;
};

bool componentTimer::init(void)
{
    LOG_INFO("componentTimer init");
    m_timer_ctx = erpc::erpc_timer_start(3000, true, std::bind(&componentTimer::timerHandler, this, std::placeholders::_1, std::placeholders::_2), nullptr);
    return true;
}

void componentTimer::exit(void)
{
    LOG_INFO("componentTimer exit");
}

void componentTimer::timerHandler(erpc::erpc_timer_t handle, void *arg)
{
    static int timer_count = 0;
    if(timer_count++ > 3)
    {
        LOG_INFO("cycle timer stop");
        erpc::erpc_timer_stop(handle);
        return ;
    }
    LOG_INFO("cycle timer running...");
}

COMPONENT_REGISTER(componentTimer);
