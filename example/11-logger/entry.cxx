#include <thread>
#include "config.h"
#include "spd_logger.hpp"
#include "simpost_application.hpp"

static void logger_routine(void)
{
    LOG_INFO("fmt format: {}", "Hello world!");
    PLOG_INFO("print format: %s", "Hello world!");
    SLOG_INFO() << "stream format " << "Hello world!";
}

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit(argv[1]))
        return -1;
    ///< 3. init business task
    std::thread logger(logger_routine);
    logger.detach();
    ///< 4. application run
    return app.exec();
}
