#include "erpc/erpc.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class server: public simpost::Component
{
public:
    explicit server() : Component("server", "V0.1.0") {}
    virtual ~server() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void reportHandler(const google::protobuf::Any &args);
};

bool server::init(void)
{
    LOG_INFO("server init");
    erpc::erpc_report_subscribe(simpost::protocol::MODULE_ID::MODULE_ID_POWER, 10, 
                                std::bind(&server::reportHandler, this, std::placeholders::_1));
    return true;
}

void server::exit(void)
{
    erpc::erpc_report_unsubscribe(simpost::protocol::MODULE_ID::MODULE_ID_POWER, 10);
    LOG_INFO("server exit");
}

void server::reportHandler(const google::protobuf::Any &args)
{
    google::protobuf::Int32Value value;
    args.UnpackTo(&value);
    LOG_INFO("receive report data: {}", value.value());
}

COMPONENT_REGISTER(server);
