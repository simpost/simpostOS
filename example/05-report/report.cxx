#include <thread>
#include "erpc/erpc.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class reporter: public simpost::Component
{
public:
    explicit reporter() : Component("reporter", "V0.1.0") {}
    virtual ~reporter() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void reportRoutine(void);
};

bool reporter::init(void)
{
    LOG_INFO("reporter init");
    std::thread reporter([this]{ reportRoutine(); });
    reporter.detach();
    return true;
}

void reporter::exit(void)
{
    LOG_INFO("reporter exit");
}

void reporter::reportRoutine(void)
{
    int32_t count = 1;
    google::protobuf::Any args;
    google::protobuf::Int32Value value;
    while (true)
    {
        value.set_value(count++);
        args.PackFrom(value);
        std::this_thread::sleep_for(std::chrono::seconds(2));
        erpc::erpc_report_publish("service", simpost::protocol::MODULE_ID::MODULE_ID_POWER, 10, args);
    }
}

COMPONENT_REGISTER(reporter);
