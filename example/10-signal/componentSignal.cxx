#include <signal.h>
#include "erpc/erpc.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"

class componentSignal: public simpost::Component
{
public:
    explicit componentSignal() : Component("moduleSignal", "V0.1.0") {}
    virtual ~componentSignal() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void signalHandler(erpc::erpc_signal_t handle, int signo, void *arg);
private:
    erpc::erpc_signal_t m_signal_ctx;
};

bool componentSignal::init(void)
{
    LOG_INFO("componentSignal init");
    m_signal_ctx = erpc::erpc_signal_register(SIGUSR1, std::bind(&componentSignal::signalHandler, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3), nullptr);
    return true;
}

void componentSignal::exit(void)
{
    LOG_INFO("componentSignal exit");
}

void componentSignal::signalHandler(erpc::erpc_signal_t handle, int signo, void *arg)
{
    LOG_INFO("recv system signal: {}", signo);
}

COMPONENT_REGISTER(componentSignal);
