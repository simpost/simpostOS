#include "erpc/erpc.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class demoService: public simpost::Component
{
public:
    explicit demoService() : Component("demoService", "V1.2.0") {}
    virtual ~demoService() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void testService(const google::protobuf::Any &args, simpost::protocol::responseMessage &response);
};

bool demoService::init(void)
{
    LOG_INFO("demoService init");
    erpc::erpc_service_register(simpost::protocol::MODULE_ID_AUDIO, 10, std::bind(&demoService::testService, this, std::placeholders::_1, std::placeholders::_2));
    erpc::erpc_service_register(simpost::protocol::MODULE_ID_AUDIO, 13, std::bind(&demoService::testService, this, std::placeholders::_1, std::placeholders::_2));
    erpc::erpc_service_register(simpost::protocol::MODULE_ID_AUDIO, 15, std::bind(&demoService::testService, this, std::placeholders::_1, std::placeholders::_2));
    return true;
}

void demoService::exit(void)
{
    LOG_INFO("demoService exit");
    erpc::erpc_service_unregister(simpost::protocol::MODULE_ID_AUDIO, 10);
    erpc::erpc_service_unregister(simpost::protocol::MODULE_ID_AUDIO, 13);
}

void demoService::testService(const google::protobuf::Any &args, simpost::protocol::responseMessage &response)
{
    google::protobuf::Int32Value intValue;
    google::protobuf::StringValue strValue;
    strValue.set_value("Hello world!");
    response.set_result(0);
    response.mutable_arg()->PackFrom(strValue);
    args.UnpackTo(&intValue);
    LOG_INFO("test service called: {}", intValue.value());
    if(intValue.value())
        std::this_thread::sleep_for(std::chrono::milliseconds(intValue.value()));
}

COMPONENT_REGISTER(demoService);
