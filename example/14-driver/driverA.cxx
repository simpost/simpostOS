#include "spd_logger.hpp"
#include "simpost_driver.hpp"

class driverA: public simpost::Driver
{
public:
    explicit driverA(): Driver("Driver-A", "V0.1.0") {}
    virtual ~driverA() = default;
    virtual int open(void) override final;
    virtual void close(void);
    virtual int ioctl(int32_t request, void *arg);
    virtual int read(uint8_t *data, int32_t size);
    virtual int write(uint8_t *data, int32_t size);
};

int driverA::open(void) {
    LOG_INFO("driver A open.");
    return 0;
}

void driverA::close(void) {
    LOG_INFO("driver A close.");
}

int driverA::ioctl(int32_t request, void *arg) {
    LOG_INFO("driver A ioctl : {}", request);
    return 0;
}

int driverA::read(uint8_t *data, int32_t size) {
    LOG_INFO("driver A read.");
    return 0;
}

int driverA::write(uint8_t *data, int32_t size) {
    LOG_INFO("driver A write.");
    return 0;
}

DRIVER_REGISTER(driverA);
