#include "spd_logger.hpp"
#include "simpost_driver.hpp"

class driverB: public simpost::Driver
{
public:
    explicit driverB(): Driver("Driver-B", "V0.1.0") {}
    virtual ~driverB() = default;
    virtual int open(void);
    virtual void close(void);
    virtual int ioctl(int32_t request, void *arg);
    virtual int read(uint8_t *data, int32_t size);
    virtual int write(uint8_t *data, int32_t size);
};

int driverB::open(void) {
    LOG_INFO("driver B open.");
    return 0;
}

void driverB::close(void) {
    LOG_INFO("driver B close.");
}

int driverB::ioctl(int32_t request, void *arg) {
    LOG_INFO("driver B ioctl : {}", request);
    return 0;
}

int driverB::read(uint8_t *data, int32_t size) {
    LOG_INFO("driver B read.");
    return 0;
}

int driverB::write(uint8_t *data, int32_t size) {
    LOG_INFO("driver B write.");
    return 0;
}

DRIVER_REGISTER(driverB);
