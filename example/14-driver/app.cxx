#include <thread>
#include "spd_logger.hpp"
#include "simpost_component.hpp"
#include "simpost_application.hpp"

class app: public simpost::Component
{
public:
    explicit app() : Component("Application", "V0.1.0") {}
    virtual ~app() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void routine(void);
};

bool app::init(void) {
    std::thread application([this]{ routine(); });
    application.detach();
    return true;
}

void app::exit(void) {
}

void app::routine(void) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    LOG_INFO("call driver for test:");
    simpost::Driver *driver = simpost::Application::getDriver("Driver-B");
    if(nullptr == driver) {
        LOG_ERROR("get driver B failed!");
        return ;
    }
    uint8_t data[64];
    driver->open();
    driver->ioctl(6, nullptr);
    driver->read(data, 64);
    driver->write(data, 64);
    driver->close();

    driver = simpost::Application::getDriver("Driver-A");
    if(nullptr == driver) {
        LOG_ERROR("get driver A failed!");
        return ;
    }
    driver->open();
    driver->ioctl(6, nullptr);
    driver->read(data, 64);
    driver->write(data, 64);
    driver->close();
}

COMPONENT_REGISTER(app);
