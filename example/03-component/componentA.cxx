#include "spd_logger.hpp"
#include "simpost_component.hpp"

class componentA: public simpost::Component
{
public:
    explicit componentA() : Component("moduleA", "V0.1.0") {}
    virtual ~componentA() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool componentA::init(void)
{
    LOG_INFO("componentA init");
    return true;
}

void componentA::exit(void)
{
    LOG_INFO("componentA exit");
}

COMPONENT_REGISTER(componentA);
