#include "spd_logger.hpp"
#include "simpost_component.hpp"

class componentB: public simpost::Component
{
public:
    explicit componentB() : Component("moduleB", "V0.1.0") {}
    virtual ~componentB() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool componentB::init(void)
{
    LOG_INFO("componentB init");
    return true;
}

void componentB::exit(void)
{
    LOG_INFO("componentB exit");
}

COMPONENT_REGISTER(componentB);
