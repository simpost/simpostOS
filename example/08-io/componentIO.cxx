#include "erpc/erpc.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"

class componentIO: public simpost::Component
{
public:
    explicit componentIO() : Component("moduleIO", "V0.1.0") {}
    virtual ~componentIO() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void ioHandler(erpc::erpc_io_t handle, int fd, void *arg);
private:
    erpc::erpc_io_t m_io_ctx;
};

bool componentIO::init(void)
{
    LOG_INFO("componentIO init");
    m_io_ctx = erpc::erpc_io_register(STDIN_FILENO, std::bind(&componentIO::ioHandler, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3), nullptr);
    return true;
}

void componentIO::exit(void)
{
    erpc::erpc_io_unregister(m_io_ctx);
    LOG_INFO("componentIO exit");
}

void componentIO::ioHandler(erpc::erpc_io_t handle, int fd, void *arg)
{
    char buffer[256];
    if(read(fd, buffer, 256) > 0)
        LOG_INFO("stdio input: {}", (char *)buffer);
}

COMPONENT_REGISTER(componentIO);
