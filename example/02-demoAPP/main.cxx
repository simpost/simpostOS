#include <thread>
#include "config.h"
#include "spd_logger.hpp"
#include "simpost_application.hpp"

static void helloworld_routine(void)
{
    int32_t count = 0;
    if(false == simpost::Application::isInit())
    {
        LOG_ERROR("framework has not been init!");
        return ;
    }

    LOG_INFO("version: {}", simpost::Application::version());
    LOG_INFO("processname: {}", simpost::Application::processname());
    LOG_INFO("workspace: {}", simpost::Application::workspace());

    while (true)
    {
        if(count++ > 10)
            break;
        std::this_thread::sleep_for(std::chrono::seconds(2));
        LOG_INFO("Hello, world!");
    }
    simpost::Application::terminate();
}

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit(argv[1]))
        return -1;
    std::thread helloworld(helloworld_routine);
    helloworld.detach();
    ///< 3. application run
    return app.exec();
}
