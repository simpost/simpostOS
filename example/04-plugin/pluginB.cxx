#include "spd_logger.hpp"
#include "simpost_component.hpp"

class pluginB: public simpost::Component
{
public:
    explicit pluginB() : Component("moduleB", "V0.1.0") {}
    virtual ~pluginB() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool pluginB::init(void)
{
    LOG_INFO("pluginB init");
    return true;
}

void pluginB::exit(void)
{
    LOG_INFO("pluginB exit");
}

PLUGIN_REGISTER(pluginB);
