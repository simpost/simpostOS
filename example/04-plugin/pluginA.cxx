#include "spd_logger.hpp"
#include "simpost_component.hpp"

class pluginA: public simpost::Component
{
public:
    explicit pluginA() : Component("moduleA", "V0.1.0") {}
    virtual ~pluginA() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool pluginA::init(void)
{
    LOG_INFO("pluginA init");
    return true;
}

void pluginA::exit(void)
{
    LOG_INFO("pluginA exit");
}

PLUGIN_REGISTER(pluginA);
