#include <thread>
#include "config.h"
#include "spd_logger.hpp"
#include "simpost_component.hpp"

class crashDemo: public simpost::Component
{
public:
    explicit crashDemo() : Component("crashDemo", SERVICE_VERSION_STR) {}
    virtual ~crashDemo() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    static void dump_routine(void);
};

void crashDemo::dump_routine(void)
{
    volatile int* a = (int*)(NULL);
    std::this_thread::sleep_for(std::chrono::microseconds(5));
    LOG_INFO("crash here");
    *a = 1;
}

bool crashDemo::init(void)
{
    LOG_INFO("crashDemo init");
    std::thread crash(dump_routine);
    crash.detach();
    return true;
}

void crashDemo::exit(void)
{
    LOG_INFO("crashDemo exit");
}

COMPONENT_REGISTER(crashDemo);
