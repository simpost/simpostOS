#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit(argv[1]))
        return -1;
    ///< 3. backtrace enable
    if(false == app.traceEnable())
        return -1;
    ///< 4. component init
    if(false == app.componentInit())
        return -1;
    ///< 5. application run
    return app.exec();
}
