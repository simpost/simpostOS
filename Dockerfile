FROM ubuntu:20.04

MAINTAINER konishi "konishi5202@163.com"

ENV TZ="Asia/Shanghai"
ENV DEBIAN_FRONTEND="noninteractive"

RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

RUN apt-get update && apt-get -y install --assume-yes dialog && apt-get -y install apt-utils whiptail && apt-get -y install openssh-server && echo "PermitRootLogin yes" >> /etc/ssh/sshd_config && echo "root:root123" | chpasswd
RUN apt-get -y install tzdata && ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime && echo ${TZ} > /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata
RUN apt-get -y install unzip zip libtool texinfo wget curl ssh telnet vim git gperf openssl gcc g++ bc gdb make dos2unix autoconf gnupg2 rsync libssl-dev u-boot-tools autopoint build-essential python3.8 python3-pip
RUN apt-get -y install locales fonts-wqy-microhei language-pack-zh*
RUN locale-gen en_US en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 && locale
RUN apt-get autoremove && apt-get clean all
RUN mkdir -p /opt/cmake && wget -P /opt/cmake https://cmake.org/files/v3.24/cmake-3.24.4-linux-x86_64.tar.gz && cd /opt/cmake && tar -xvf cmake-3.24.4-linux-x86_64.tar.gz && rm cmake-3.24.4-linux-x86_64.tar.gz
RUN mkdir -p /opt/protoc/protoc-21.12 && wget -P /opt/protoc https://github.com/protocolbuffers/protobuf/releases/download/v21.12/protoc-21.12-linux-x86_64.zip && cd /opt/protoc && unzip protoc-21.12-linux-x86_64.zip -d /opt/protoc/protoc-21.12 && rm protoc-21.12-linux-x86_64.zip

RUN useradd -ms /bin/bash konishi
USER konishi

WORKDIR /home/autel
ENV USER autel
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PATH=/opt/cmake/cmake-3.24.4-linux-x86_64/bin:/opt/protoc/protoc-21.12/bin:$PATH

CMD ["/bin/bash"]