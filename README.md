# simpostOS #

## 一、版本说明

1. `V0.1.0`：支持产品内部所有业务交互，不支持产品间业务交互，支持各种`MCU`接入；
2. `V0.2.x`：支持产品之间的业务，配合`msgProxy`可实现`uart`或`UDP`跨网段的产品间业务交互；
3. `V0.4.x`：统一内部协议、通信中间件、架构等模块的版本，简化整体软件实现，增加`report`单向通信机制，实现时间同步、升级服务通用能力；

后续版本规划：
- 支持一个进程多`reader`、`writer`方法的业务交互；
- `rosProxy`：该代理服务实现`ROS2`网络数据到`simpostOS`网络数据的转换。
- 裁剪版本专用于各种`MCU`和单片机系统；

## 二、使用手册

01. [系统介绍](./docs/01-introduce.md)
02. [开始之前](./docs/02-beforeStart.md)
03. [快速入门](./docs/03-quickStart.md)
04. [你好，世界](./docs/04-helloworld.md)
05. [第一个应用程序](./docs/05-demoAPP.md)
06. [第一个组件程序](./docs/06-component.md)
07. [第一个插件程序](./docs/07-plugin.md)
08. [上报单向通信](./docs/08-report.md)
09. [服务端与客户端](./docs/09-service.md)
10. [发布者与订阅者](./docs/10-topic.md)
11. [程序异常崩溃分析](./docs/11-crash.md)
12. [多路`io`复用](./docs/12-io.md)
13. [通用定时器](./docs/13-timer.md)
14. [信号处理程序](./docs/14-signal.md)
15. [日志接口介绍](./docs/15-logger.md)
16. [自定义通信方法](./docs/16-communicate.md)
17. [组件部署规则](./docs/17-deployment.md)
18. [调试方法](./docs/18-debug.md)
