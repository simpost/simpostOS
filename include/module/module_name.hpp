#ifndef __MODULE_NAME_HPP__
#define __MODULE_NAME_HPP__
#include "module_identity.pb.h"

namespace simpost
{

namespace protocol
{

extern std::string &module_name(int32_t identity);

}

}

using namespace simpost::protocol;

#endif
