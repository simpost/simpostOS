#ifndef __ERPC_REPORT_HPP__
#define __ERPC_REPORT_HPP__
#include <string>
#include <functional>
#include "google/protobuf/any.pb.h"

namespace erpc
{

using report_handler_t = std::function<void(const google::protobuf::Any &args)>;

extern int erpc_report_subscribe(const int32_t module, const int32_t report, const report_handler_t handler);
extern void erpc_report_unsubscribe(const int32_t module, const int32_t report);
extern int erpc_report_publish(const std::string &uid, const int32_t module, const int32_t report, const google::protobuf::Any &args);

}

#endif  // __ERPC_REPORT_HPP__
