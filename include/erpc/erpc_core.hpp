#ifndef __ERPC_CORE_HPP__
#define __ERPC_CORE_HPP__
#include <string>

namespace erpc
{

extern bool is_init(void);
extern int  erpc_exec(void);
extern void erpc_terminate(int status);
extern bool erpc_init(const std::string &profile);

extern const std::string erpc_name(void);
extern const std::string erpc_version(void);
extern const std::string erpc_version_description(void);

extern const int32_t erpc_message_size(void);
extern const std::string &erpc_workspace(void);
extern const std::string &erpc_process_name(void);

extern const std::string &erpc_get_uid(void);
extern void erpc_set_uid(const std::string &uid);

}

#endif // __ERPC_CORE_HPP__
