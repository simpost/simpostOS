#ifndef __ERPC_DRIVER_HPP__
#define __ERPC_DRIVER_HPP__
#include <string>
#include <functional>

namespace erpc
{

using sender_handler_t = std::function<void(const uint8_t *, size_t)>;

extern void erpc_sender_unregister(const std::string &dst_uid);
extern bool erpc_sender_register(const std::string &dst_uid, sender_handler_t sender);
extern void erpc_router_unregister(const std::string &dst_uid);
extern bool erpc_router_register(const std::string &dst_uid, std::string &addr, uint16_t port);
extern void erpc_dispatch_message(const uint8_t *data, size_t len);

extern bool erpc_source_valid_check(const std::string &src, const std::string &addr, const std::string &mask);

}

#endif // __ERPC_DRIVER_HPP__
