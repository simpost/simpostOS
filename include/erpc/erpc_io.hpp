#ifndef __ERPC_IO_HPP__
#define __ERPC_IO_HPP__
#include <functional>

namespace erpc
{

using erpc_io_t = void *;
using erpc_io_handler_t = std::function<void(erpc_io_t, int, void *)>;

extern erpc_io_t erpc_io_register(int fd, erpc_io_handler_t handler, void *arg);
extern void erpc_io_unregister(erpc_io_t handle);

}

#endif // __ERPC_IO_HPP__
