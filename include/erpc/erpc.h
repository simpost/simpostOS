#ifndef __ERPC_H__
#define __ERPC_H__

#include "erpc_core.hpp"
#include "erpc_topic.hpp"
#include "erpc_report.hpp"
#include "erpc_service.hpp"

#include "erpc_io.hpp"
#include "erpc_timer.hpp"
#include "erpc_signal.hpp"

#include "erpc_driver.hpp"    ///< for message router and dispatcher
#include "rpc_response.pb.h"  ///< for response message

#endif // __ERPC_H__
