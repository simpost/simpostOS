#ifndef __ERPC_TIMER_HPP__
#define __ERPC_TIMER_HPP__
#include <functional>

namespace erpc
{

using erpc_timer_t = void *;
using erpc_timer_handler_t = std::function<void(erpc_timer_t, void *)>;

extern erpc_timer_t erpc_timer_start(uint64_t timeout_msec, bool reload, erpc_timer_handler_t handler, void *arg);
extern void erpc_timer_stop(erpc_timer_t handle);

}

#endif // __ERPC_TIMER_HPP__
