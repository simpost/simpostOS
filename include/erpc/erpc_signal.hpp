#ifndef __ERPC_SIGNAL_HPP__
#define __ERPC_SIGNAL_HPP__
#include <functional>

namespace erpc
{

using erpc_signal_t = void *;
using erpc_signal_handler_t = std::function<void(erpc_signal_t, int, void *)>;

extern erpc_signal_t erpc_signal_register(int signo, erpc_signal_handler_t handler, void *arg);
extern void erpc_signal_unregister(erpc_signal_t handle);

}

#endif // __ERPC_SIGNAL_HPP__
