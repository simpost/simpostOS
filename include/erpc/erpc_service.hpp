#ifndef __ERPC_SERVICE_HPP__
#define __ERPC_SERVICE_HPP__
#include <string>
#include <functional>
#include "rpc_response.pb.h"

namespace erpc
{

using namespace simpost::protocol;
using service_handler_t = std::function<void(const google::protobuf::Any &args, responseMessage &response)>;

extern int erpc_service_register(const int32_t module, const int32_t service, const service_handler_t handler);
extern void erpc_service_unregister(const int32_t module, const int32_t service);
extern responseMessage erpc_service_call(const std::string &uid, const int32_t module, const int32_t service, const google::protobuf::Any &args, const int msec);

}

#endif // __ERPC_SERVICE_HPP__
