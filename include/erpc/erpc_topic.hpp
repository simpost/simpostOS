#ifndef __ERPC_TOPIC_HPP__
#define __ERPC_TOPIC_HPP__
#include <string>
#include <functional>
#include <google/protobuf/any.pb.h>

namespace erpc
{

using topic_fetcher_t = std::function<void(google::protobuf::Any &args)>;
using topic_handler_t = std::function<void(const google::protobuf::Any &args)>;

extern int erpc_topic_create(const int32_t publisher, const int32_t topic, topic_fetcher_t fetcher);
extern void erpc_topic_destroy(const int32_t publisher, const int32_t topic);
extern bool erpc_topic_is_active(const std::string &uid, const int32_t subscriber, const int32_t topic);
extern int erpc_topic_publish(const int32_t publisher, const int32_t topic, const google::protobuf::Any &args);
extern int erpc_topic_unsubscribe(const std::string &uid, const int32_t subscriber, const int32_t publisher, const int32_t topic);
extern int erpc_topic_subscribe(const std::string &uid, const int32_t subscriber, const int32_t publisher, const int32_t topic, const topic_handler_t handler);
extern bool erpc_topic_fetch(const std::string &uid, const int32_t publisher, const int32_t topic, google::protobuf::Any &data, const int msec);

extern void erpc_subscriber_remove(const int32_t publisher, const int32_t topic, const std::string &uid, const int32_t subscriber);

}

#endif // __ERPC_TOPIC_HPP__
