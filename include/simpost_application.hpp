#ifndef __SIMPOST_APPLICATION_HPP__
#define __SIMPOST_APPLICATION_HPP__
#include <string>
#include <functional>
#include "simpost_driver.hpp"

namespace simpost
{

typedef enum {
    LOG_TYPE_FILE = 0,
    LOG_TYPE_CONSOLE = 1,
}log_type_e;

using signal_t = void *;
using signal_handler_t = std::function<void(signal_t, int, void *)>;

class Application
{
public:
    explicit Application(int argc, char *argv[], const std::string &version = "");
    virtual ~Application();
    int exec(void);
    bool traceEnable(void);
    bool logInit(log_type_e type, const std::string &codeState = "", const std::string &buildEnv = "");
    bool coreInit(const std::string &profile = "");
public:
    static bool driverInit(void);
    static void driverEixt(void);
    static Driver *getDriver(const std::string &name);
public:
    static bool pluginInit(void);
    static void pluginExit(void);
public:
    static bool componentInit(void);
    static void componentExit(void);
public:
    static bool signalRegister(int signo, signal_handler_t handler, void *arg);
    static void signalUnregister(signal_t handle);
public:
    static bool isInit(void);
    static void terminate(void);
    static const std::string version(void);
    static const std::string &workspace(void);
    static const std::string &processname(void);
};

}

#endif // 