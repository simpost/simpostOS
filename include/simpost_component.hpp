#ifndef __SIMPOST_COMPONENT_HPP__
#define __SIMPOST_COMPONENT_HPP__
#include <string>

namespace simpost
{

class Component
{
public:
    explicit Component(const std::string &name, const std::string &version) : m_name(std::move(name)), m_version(std::move(version)) {}
    virtual ~Component() = default;
    const std::string &name(void) { return m_name; }
    const std::string &version(void) { return m_version; }
    virtual bool init(void) = 0;
    virtual void exit(void) = 0;

private:
    std::string m_name;
    std::string m_version;
};

extern void component_register(Component *component);

};

#define PLUGIN_REGISTER(pluginClass) \
        extern "C" { \
            simpost::Component *pluginCreate(void) { \
                return new pluginClass; \
            } \
            void pluginDestroy(simpost::Component *obj) { \
                delete obj; \
            } \
        }

#define COMPONENT_REGISTER(component) \
        namespace { \
            struct ComponentRegister { \
                ComponentRegister() { \
                    static component _instance; \
                    simpost::component_register(&_instance); \
                } \
            }; \
            static ComponentRegister component##_instance; \
        }

#endif // __SIMPOST_COMPONENT_HPP__
