#ifndef __SIMPOST_DRIVER_HPP__
#define __SIMPOST_DRIVER_HPP__
#include <string>

namespace simpost
{

class Driver
{
public:
    explicit Driver(const std::string &name, const std::string &version): m_name(std::move(name)), m_version(std::move(version)) {}
    virtual ~Driver() = default;
    const std::string &name(void) { return m_name; }
    const std::string &version(void) { return m_version; }
    virtual int open(void) = 0;
    virtual void close(void) = 0;
    virtual int ioctl(int32_t request, void *arg) = 0;
    virtual int read(uint8_t *data, int32_t size) = 0;
    virtual int write(uint8_t *data, int32_t size) = 0;
private:
    std::string m_name;
    std::string m_version;
};

}

#define DRIVER_REGISTER(pluginClass) \
        extern "C" { \
            simpost::Driver *pluginCreate(void) { \
                return new pluginClass; \
            } \
            void pluginDestroy(simpost::Driver *obj) { \
                delete obj; \
            } \
        }

#endif // __SIMPOST_DRIVER_HPP__
