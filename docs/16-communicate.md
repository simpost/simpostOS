# 自定义通信方法 #

为了满足多种通信方式的需求（比如`UART`、`USB`、`TCP`等），添加自定义通信方法的示例。本示例程序基于前面组件式编程基础上演变而来。

## 一、实现主程序

与上报示例程序主程序类似，我们增加网络部署的初始化（既然要定义收发方法，那么就是基于网络通信，同时也依赖于`deployment.yeml`网络部署配置文件）：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

## 二、自定义发送程序

### 2.1 添加路由配置

`simpostOS`支持静态和动态路由两种配置方法。

#### 2.1.1 添加静态路由配置

静态路由配置通过加载`deployment.yaml`文件完成，配置文件说明详见[组件部署规则](./17-deployment.md)。这里重点强调一下适用于通信代理的万能配置：

```
dependService:
  mcuProxy:
    uid: OUTUID_ANY
    port: 17050
    addr: 127.0.0.1
```

当有业务消息发送，且没有匹配的`dst_uid`时，消息统一发送到`127.0.0.1:17050`的`mcuProxy`处进行转发。

#### 2.1.2 添加动态路由配置

所谓动态路由配置，其实就是动态的向`simpostOS`中添加或删除路由配置项，主要使用下面两个接口：

```
#include "erpc/erpc_driver.hpp"

extern void erpc_router_unregister(const std::string &dst_uid);
extern bool erpc_router_register(const std::string &dst_uid, std::string &addr, uint16_t port);
```

- `dst_uid`：是进程/服务的唯一标识；
- `addr`：是进程/服务的网络地址；
- `port`：是进程/服务的网络端口。

要实现动态路由的配置，用户可以自定义网络分配和网络发现协议，然后使用此方法动态更新路由信息，实现上层业务的可达。

注意：添加路由配置的方法仅适用于使用`UDP`的通信的方式。

### 2.2 添加特定的发送方法

在智能硬件开发场景中，产品由多颗芯片（`SoC`、`ECU`、`MCU`等）构成，芯片之间经常使用`UART`、`USB`等通信方式连接。那么，跨芯片的业务交互，就不是简单的`UDP`网络通信了。针对这个场景，`simpostOS`提供为特定的`uid`自定义发送方法，主要使用如下两个接口：

```
#include "erpc/erpc_driver.hpp"

using sender_handler_t = std::function<void(const uint8_t *, size_t)>;

extern void erpc_sender_unregister(const std::string &dst_uid);
extern bool erpc_sender_register(const std::string &dst_uid, sender_handler_t sender);
```

- `dst_uid`：是进程/服务的唯一标识；
- `sender`：是消息发送的方法回调；当业务消息是发送到`dst_uid`时，该函数就会被调用。

### 2.3 使用`UDP`实现发送方法示例

下面是采用组件式编程方法，用`UDP`实现的一个自定义特定发送方法的示例：

```
#include <thread>
#include "erpc/erpc.h"
#include "simpost_component.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>

class sender: public simpost::Component
{
public:
    explicit sender() : Component("sender", "V1.0.0") {}
    virtual ~sender() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void app_task(void);
    bool socket_init(const std::string &addr, uint16_t port);
    static void udp_sender(const uint8_t *data, size_t length);
private:
    static int m_sockfd;
    static sockaddr_in m_addr;
};

int sender::m_sockfd = -1;
sockaddr_in sender::m_addr;

bool sender::init(void)
{
    LOG_INFO("sender init");
    if(false == socket_init("127.0.0.1", 10089))
        return false;
    erpc::erpc_sender_register("communicate", udp_sender);
    std::thread app_thread([this]{ app_task(); });
    app_thread.detach();
    return true;
}

void sender::exit(void)
{
    LOG_INFO("sender exit");
}

void sender::app_task(void)
{
    google::protobuf::Any args;
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        LOG_INFO("sender report message");
        erpc::erpc_report_publish("communicate", 123, 456, args);
    }
}

bool sender::socket_init(const std::string &addr, uint16_t port)
{
    m_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(m_sockfd < 0)
    {
        LOG_ERROR("create udp socket failed!");
        return false;
    }
    bzero(&m_addr, sizeof(m_addr));
    m_addr.sin_family = AF_INET;
    m_addr.sin_port = htons(port);
    m_addr.sin_addr.s_addr = inet_addr(addr.c_str());
    return true;
}

void sender::udp_sender(const uint8_t *data, size_t length)
{
    sendto(m_sockfd, data, length, 0, (struct sockaddr *)&m_addr, sizeof(struct sockaddr_in));
}

COMPONENT_REGISTER(sender);
```

- 在组件初始化时，首先配置发送目的地为`127.0.0.1:10089`，并调用`erpc::erpc_sender_register("communicate", udp_sender)`方法，注册了`dst_uid`为`communicate`时的发送方法为`udp_sender()`；
- 启动业务任务`app_task()`，每隔2秒调用`erpc::erpc_report_public()`方法，向`communicate`对象的`123`模块的`456`方法发送数据。

## 三、自定义接收程序

与发送程序对应，`simpostOS`还提供了三种自定义的消息接收处理方法，针对`UDP`的通信方法，还提供设置过滤器的方式。

### 3.1 静态配置`UDP`接收方法和过滤器

在`deployment.yaml`配置文件的`basicConfig`配置中，可以通过修改`port`配置来设置`UDP`消息的接收端口，还可以配置`addr`和`mask`来设置`UDP`来源的过滤器，配置`maxMessageSize`修改消息通信的缓冲大小：

```
basicConfig:             # 服务自身配置
  uid: service           # 必选，服务唯一编号
  listenPort: 17010      # 必选，服务数据出入端口
  timeout: 5ms           # 可选，服务的默认超时配置，单位可以是ms、s 或 min
  timecycle: 3S          # 可选，服务的周期时间，单位可以是ms、s 或 min
  msgDebug: true         # 可选，打印通信消息信息便于调试
  minThreads: 3          # 可选，服务的最小线程池个数
  maxThreads: 5          # 可选，服务的最大线程池个数
  maxWorkSize: 100       # 可选，服务的最大任务队列数
  maxMessageSize: 4096   # 可选，消息的最大数据长度
```

同时，针对`UDP`通信方式，`simpostOS`还提供动态修改来源过滤器的方法：

```
#include "erpc/erpc_driver.hpp"

extern void erpc_receiver_filter(std::string &addr, std::string &mask);
```

调用该方法后，将会覆盖`deployment.yaml`中的过滤器配置。针对自定义的`UDP`接收器（一个程序监听多个`UDP`端口的情况），也可以调用如下接口来判断接收来源是否满足要求：

```
#include "erpc/erpc_driver.hpp"

extern bool erpc_source_valid_check(const std::string &src, const std::string &addr, const std::string &mask);
```

- `src`是当前数据的来源地址；
- `addr`是可接收数据的来源地址，也即期望的地址；
- `mask`是可接收数据的地址掩码，用于计算可信数据源地址范围。

### 3.2 自定义接收程序

此方法适用于`UART`、`USB`、`TCP`或增加多个`UDP`接收数据的通信场景；在`UART`、`USB`、`TCP`场景时，还需要同步定义发送方法。实现自定义接收程序的步骤如下：

1. 配置并建立通信方式，比如配置`UART`、`USB`,或者配置`TCP`客户端并建立链接，或者配置`TCP`服务端并等待新的链接，注意**推荐配置为非阻塞访问**；
2. 调用`erpc::erpc_io_register()`异步方法，将第一步创建的异步`IO`添加到`simpostOS`的观察列表中，并指定数据读取的回调函数；
3. 在第二步指定的数据读取回调函数中，首先调用对应的数据读取函数（比如`UART`读取函数、`USB`读取函数、`TCP`或`UDP`读取函数）获取数据，然后调用下面接口将数据加入`simpostOS`的业务处理：

```
#include "erpc/erpc_driver.hpp"

extern void erpc_dispatch_message(const uint8_t *data, size_t len);
```

注意：传入`erpc_dispatch_message()`函数的数据，需要保障是完整的一帧数据，否则数据会解析失败。

### 3.3 使用`UDP`实现接收方法示例

下面是采用组件式编程方法，用`UDP`实现的一个自定义接收方法的示例：

```
#include "erpc/erpc.h"
#include "simpost_component.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>

class receiver: public simpost::Component
{
public:
    explicit receiver() : Component("receiver", "V1.0.0") {}
    virtual ~receiver() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    bool socket_init(uint16_t port);
    void report_handler(const google::protobuf::Any &args);
    static void receiver_handler(erpc::erpc_io_t handle, int sockfd, void *args);
private:
    static int m_sockfd;
};

int receiver::m_sockfd = -1;

bool receiver::init(void)
{
    LOG_INFO("receiver init");
    if(false == socket_init(10089))
        return false;
    erpc::erpc_report_subscribe(123, 456, std::bind(&receiver::report_handler, this, std::placeholders::_1));
    return true;
}

void receiver::exit(void)
{
    LOG_INFO("receiver exit");
}

bool receiver::socket_init(uint16_t port)
{
    int reuse = 1;
    struct sockaddr_in addr;
    m_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(m_sockfd < 0)
    {
        LOG_ERROR("create socket error: {}", strerror(errno));
        return false;
    }
    if(setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
    {
        LOG_ERROR("setsockopt add reuse error: {}", strerror(errno));
        close(m_sockfd);
        return false;
    }
    if(fcntl(m_sockfd, F_SETFL, fcntl(m_sockfd, F_GETFL) | O_NONBLOCK) < 0)
    {
        LOG_ERROR("fcntl set nonblock error: {}", strerror(errno));
        close(m_sockfd);
        return false;
    }
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htons(INADDR_ANY);
    if(bind(m_sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0)
    {
        LOG_ERROR("bind socket error: {}", strerror(errno));
        close(m_sockfd);
        return false;
    }
    erpc::erpc_io_register(m_sockfd, receiver_handler, nullptr);
    return true;
}

void receiver::report_handler(const google::protobuf::Any &args)
{
    LOG_INFO("receive a report message");
}

void receiver::receiver_handler(erpc::erpc_io_t handle, int sockfd, void *args)
{
    int result = -1;
    uint8_t buffer[1024];
    struct sockaddr_in addr_in;
    socklen_t addrLen = sizeof(struct sockaddr_in);
    result = recvfrom(m_sockfd, buffer, 1024, 0, (struct sockaddr *)&addr_in, &addrLen);
    if(result < 0)
    {
        LOG_ERROR("recv data error: {}", strerror(errno));
        return ;
    }
    erpc::erpc_dispatch_message(buffer, result);
}

COMPONENT_REGISTER(receiver);
```

## 四、数据流通信如何保障一帧数据

针对`UART`、`USB`、`TCP`等流式通信方式，站在应用者的角度来讲，其原始业务数据是没有所谓的帧的概念的，这就需要我们为其定义一套帧的格式来保障数据按照一帧、一帧的形式进行收发。比如下面的帧格式：

![image](./images/stream_frame_format.png)

那么在自定义特定发送方法的函数中，就可以在一帧数据前面，增加报文首部数据内容，并先发送报文首部，再发送业务数据内容；而在自定义的特定数据接收方法中，按照此帧格式解析数据，然后将正确解析出的完整业务数据内容灌入`erpc::erpc_dispatch_message()`函数，调动`simpostOS`为我们处理业务。
