# 服务端与客户端示例 #

本示例程序基于前面组件式编程基础上演变而来。

## 一、实现主程序

与上报示例程序主程序一样，源码如下，其中日志和框架初始化使用了默认参数：

```
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

## 二、实现服务组件程序

创建服务组件程序流程如下：

1. 参照组件编程流程，实现组件的正确注册与注销；
2. 包含`erpc/erpc.h`和`module/module_identity.pb.h`头文件；
3. 在`init()`函数中，使用`erpc::erpc_service_register()`注册服务回调函数；
4. 在`exit()`函数中，使用`erpc::erpc_service_unregister()`注销服务；

下面是一个服务组件程序完整源码：

```
#include "erpc/erpc.h"
#include "module_identity.pb.h"
#include "simpost_component.hpp"
#include "google/protobuf/wrappers.pb.h"

class demoService: public simpost::Component
{
public:
    explicit demoService() : Component("demoService", "V1.2.0") {}
    virtual ~demoService() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void testService(const google::protobuf::Any &args, simpost::protocol::responseMessage &response);
};

bool demoService::init(void)
{
    LOG_INFO("demoService init");
    erpc::erpc_service_register(simpost::protocol::MODULE_ID_AUDIO, 10, std::bind(&demoService::testService, this, std::placeholders::_1, std::placeholders::_2));
    erpc::erpc_service_register(simpost::protocol::MODULE_ID_AUDIO, 13, std::bind(&demoService::testService, this, std::placeholders::_1, std::placeholders::_2));
    return true;
}

void demoService::exit(void)
{
    LOG_INFO("demoService exit");
    erpc::erpc_service_unregister(simpost::protocol::MODULE_ID_AUDIO, 10);
    erpc::erpc_service_unregister(simpost::protocol::MODULE_ID_AUDIO, 13);
}

void demoService::testService(const google::protobuf::Any &args, simpost::protocol::responseMessage &response)
{
    google::protobuf::Int32Value intValue;
    google::protobuf::StringValue strValue;
    strValue.set_value("Hello world!");
    response.set_result(0);
    response.mutable_arg()->PackFrom(strValue);
    args.UnpackTo(&intValue);
    LOG_INFO("test service called: {}", intValue.value());
    if(intValue.value())
        std::this_thread::sleep_for(std::chrono::milliseconds(intValue.value()));
}

COMPONENT_REGISTER(demoService);
```

该示例程序提供了一个服务，从参数中解析出一个整数，使用该整数进行一定的延迟，最后返回了一个字符串数据。

## 三、实现客户组件程序

创建客户组件程序流程如下：

1. 参照组件编程流程，实现组件的正确注册与注销；
2. 包含`erpc/erpc.h`和`module/module_identity.pb.h`头文件；
3. 根据业务需要，使用`erpc::erpc_service_call()`方法调用远程服务，并判断该函数的返回值。

下面是一个客户组件程序完整源码：

```
#include <thread>
#include "erpc/erpc.h"
#include "simpost_component.hpp"
#include "module/module_name.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class demoAPP: public simpost::Component
{
public:
    explicit demoAPP() : Component("demoAPP", "V1.0.0") {}
    virtual ~demoAPP() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void app_task(void);
};

bool demoAPP::init(void)
{
    LOG_INFO("demoAPP init");
    std::thread app_thread([this]{ app_task(); });
    app_thread.detach();
    return true;
}

void demoAPP::exit(void)
{
    LOG_INFO("demoAPP exit");
}

void demoAPP::app_task(void)
{
    google::protobuf::Any args;
    google::protobuf::Int32Value intValue;
    google::protobuf::StringValue strValue;
    responseMessage response;

    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(3));
        intValue.set_value(3);
        args.PackFrom(intValue);
        response = erpc::erpc_service_call("service", MODULE_ID_AUDIO, 13, args, 5);
        if(0 != response.result())
        {
            LOG_ERROR("service {}/13 error: {}, {}", module_name(MODULE_ID_AUDIO), response.err_code(), response.err_desc());
        }
        else
        {
            response.arg().UnpackTo(&strValue);
            LOG_INFO("service moduleA/13 return: {}", strValue.value());
        }

        intValue.set_value(5);
        args.PackFrom(intValue);
        response = erpc::erpc_service_call("service", MODULE_ID_AUDIO, 10, args, 10);
        if(0 != response.result())
        {
            LOG_ERROR("service {}/10 error: {}, {}", module_name(MODULE_ID_AUDIO), response.err_code(), response.err_desc());
        }
        else
        {
            response.arg().UnpackTo(&strValue);
            LOG_INFO("service 2345/10 return: {}", strValue.value());
        }

        intValue.set_value(10);
        args.PackFrom(intValue);
        response = erpc::erpc_service_call("service", MODULE_ID_AUDIO, 15, args, 10);
        if(0 != response.result())
        {
            LOG_ERROR("service {}/15 error: {}, {}", module_name(MODULE_ID_AUDIO), response.err_code(), response.err_desc());
        }
        else
        {
            response.arg().UnpackTo(&strValue);
            LOG_INFO("service moduleA/test return: {}", strValue.value());
        }
    }
}

COMPONENT_REGISTER(demoAPP);
```

在该示例中，每隔3S调用3次服务端的服务，且第一次调用延迟3毫秒、超时5毫秒；第二次调用延迟5毫秒、超时10毫秒；第三次调用延迟10毫秒、超时10毫秒。所以前两次调用会成功，第三次调用会超时。

## 四、运行示例程序

拷贝部署配置文件，并运行示例程序：

```
$ source setup.sh
$ cp example/06-service/deployment.yaml config/
$ ./bin/demoService
[2024-04-16 14:32:41.001] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 14:32:41.001] [-I-] [simpost_application.cxx:54 log()] : demoService version: V1.0.0.0
[2024-04-16 14:32:41.001] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 14:32:41.001] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:32:41.001] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:32:41.001] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:32:41.001] [-I-] [erpc_profile.cxx:110 basicConfig()] : basicConfig: service
[2024-04-16 14:32:41.001] [-I-] [erpc_profile.cxx:111 basicConfig()] : listenPort: 17011, bufferSize: 4096, messageSize: 100
[2024-04-16 14:32:41.001] [-I-] [erpc_profile.cxx:112 basicConfig()] : source mask: 127.0.0.1/255.255.255.0
[2024-04-16 14:32:41.001] [-W-] [erpc_profile.cxx:137 parseNetConfig()] : warn config: no dependService!
[2024-04-16 14:32:41.002] [-I-] [erpc_transmit.cxx:179 erpc_transmit_init()] : transmit buffer size: 4096
[2024-04-16 14:32:41.002] [-I-] [demoServer.cxx:19 init()] : demoService init
[2024-04-16 14:32:41.002] [-I-] [demoClient.cxx:21 init()] : demoAPP init
[2024-04-16 14:32:41.002] [-I-] [erpc_router.hpp:155 routine()] : message router is runing
[2024-04-16 14:32:44.003] [-I-] [demoServer.cxx:41 testService()] : test service called: 3
[2024-04-16 14:32:44.006] [-I-] [demoClient.cxx:52 app_task()] : service moduleA/13 return: Hello world!
[2024-04-16 14:32:44.006] [-I-] [demoServer.cxx:41 testService()] : test service called: 5
[2024-04-16 14:32:44.012] [-I-] [demoClient.cxx:65 app_task()] : service 2345/10 return: Hello world!
[2024-04-16 14:32:44.012] [-I-] [demoServer.cxx:41 testService()] : test service called: 10
[2024-04-16 14:32:44.022] [-E-] [erpc_service.cxx:117 erpc_service_call()] : service audio_15 call timeout, id: 2!
[2024-04-16 14:32:44.022] [-E-] [demoClient.cxx:73 app_task()] : service audio/15 error: 39, service call timeout
[2024-04-16 14:32:44.022] [-W-] [erpc_proxy.cxx:49 service_proxy_wakeup()] : service id: 2 not exist, discard!
[2024-04-16 14:32:47.022] [-I-] [demoServer.cxx:41 testService()] : test service called: 3
[2024-04-16 14:32:47.026] [-I-] [demoClient.cxx:52 app_task()] : service moduleA/13 return: Hello world!
[2024-04-16 14:32:47.026] [-I-] [demoServer.cxx:41 testService()] : test service called: 5
[2024-04-16 14:32:47.031] [-I-] [demoClient.cxx:65 app_task()] : service 2345/10 return: Hello world!
[2024-04-16 14:32:47.031] [-I-] [demoServer.cxx:41 testService()] : test service called: 10
[2024-04-16 14:32:47.041] [-E-] [erpc_service.cxx:117 erpc_service_call()] : service audio_15 call timeout, id: 5!
[2024-04-16 14:32:47.041] [-E-] [demoClient.cxx:73 app_task()] : service audio/15 error: 39, service call timeout
[2024-04-16 14:32:47.042] [-W-] [erpc_proxy.cxx:49 service_proxy_wakeup()] : service id: 5 not exist, discard!
[2024-04-16 14:32:50.042] [-I-] [demoServer.cxx:41 testService()] : test service called: 3
[2024-04-16 14:32:50.045] [-I-] [demoClient.cxx:52 app_task()] : service moduleA/13 return: Hello world!
[2024-04-16 14:32:50.045] [-I-] [demoServer.cxx:41 testService()] : test service called: 5
[2024-04-16 14:32:50.051] [-I-] [demoClient.cxx:65 app_task()] : service 2345/10 return: Hello world!
[2024-04-16 14:32:50.051] [-I-] [demoServer.cxx:41 testService()] : test service called: 10
[2024-04-16 14:32:50.061] [-E-] [erpc_service.cxx:117 erpc_service_call()] : service audio_15 call timeout, id: 8!
[2024-04-16 14:32:50.061] [-E-] [demoClient.cxx:73 app_task()] : service audio/15 error: 39, service call timeout
[2024-04-16 14:32:50.061] [-W-] [erpc_proxy.cxx:49 service_proxy_wakeup()] : service id: 8 not exist, discard!
```
