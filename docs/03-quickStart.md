# quick start #

## 一、使用`Dockerfile`构建

### 1.1 repo clone

```
$ git clone https://gitee.com/simpost/simpostOS.git
```

### 1.2 build docker image

```
$ cd simpostOS
$ docker image build -t simpostOS:v0.1 .
```

### 1.3 run docker image

```
$ docker run --name simpostOS -itd simpostOS:v0.1
```

### 1.4 build example

```
$ docker exec -it simpostOS /bin/bash
$ git clone https://gitee.com/simpost/simpostOS.git
$ cd simpostOS
$ mkdir build && cd build
$ cmake ../example
$ make && make install
```

### 1.5 run example

```
$ cd .. # in simpostOS directory
$ source setup.sh
$ ./bin/helloworld
```

## 二、自己搭建服务器环境构建

### 2.1 安装依赖软件

为了防止系统差异，带来`ABI`问题，推荐使用`ubuntu:20.04`版本系统。切换到`root`权限用于，使用`apt-get install`方法，安装如下软件：

```
# apt-get update
# apt-get -y install apt-utils whiptail && apt-get -y install openssh-server
# apt-get -y install unzip zip libtool texinfo wget curl ssh telnet vim git gperf openssl gcc g++ bc gdb make dos2unix autoconf gnupg2 rsync libssl-dev u-boot-tools autopoint build-essential python3.8 python3-pip
# apt-get -y install locales fonts-wqy-microhei language-pack-zh*
# locale-gen en_US en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 && locale
# apt-get autoremove && apt-get clean all
```

### 2.2 安装`cmake`和`protoc`工具

下载`cmake`和`protoc`工具到`/opt`目录下：

- `cmake`: https://cmake.org/files/v3.24/cmake-3.24.4-linux-x86_64.tar.gz
- `protoc`: https://github.com/protocolbuffers/protobuf/releases/download/v21.12/protoc-21.12-linux-x86_64.zip

最后，在`PATH`路径中增加`cmake`和`protoc`工具的路径：

`export PATH=/opt/cmake/cmake-3.24.4-linux-x86_64/bin:/opt/protoc/protoc-21.12/bin:$PATH`

### 2.3 构建示例程序

```
$ git clone https://gitee.com/simpost/simpostOS.git
$ cd simpostOS
$ mkdir build && cd build
$ cmake ../example
$ make && make install
```

### 2.4 运行示例程序

```
$ cd .. # in simpostOS directory
$ source setup.sh
$ ./bin/helloworld
```
