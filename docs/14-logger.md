# 日志配置文件说明 #

## 1. 接口介绍

`simpostOS`使用`spdlog`作为基础日志库，提供`trace`、`debug`、`info`、`warn`、`error`、`fatal`六个等级的日志，并向业务提供`fmt`、`print`和`stream`三种形式的接口，以满足应用开发人员的不同编码习惯。应用程序编写只需要调用如下接口即可，日志库的初始化和配置已经`simpostOS`启动后首先完成。

```
///< use fmt lib, e.g. LOG_INFO("info log, {1}, {2}, {1}", 1, 2)
#define LOG_TRACE(msg,...)      spdlog::log({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::trace, msg, ##__VA_ARGS__)
#define LOG_DEBUG(msg,...)      spdlog::log({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::debug, msg, ##__VA_ARGS__)
#define LOG_INFO(msg,...)       spdlog::log({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::info, msg, ##__VA_ARGS__)
#define LOG_WARN(msg,...)       spdlog::log({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::warn, msg, ##__VA_ARGS__)
#define LOG_ERROR(msg,...)      spdlog::log({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::err, msg, ##__VA_ARGS__)
#define LOG_FATAL(msg,...)      spdlog::log({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::critical, msg, ##__VA_ARGS__)

///< use like printf, e.g. PLOG_INFO("info log, %d-%d", 1, 2)
#define PLOG_TRACE(fmt,...)     SPDLogger::print({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::trace, fmt, ##__VA_ARGS__)
#define PLOG_DEBUG(fmt,...)     SPDLogger::print({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::debug, fmt, ##__VA_ARGS__)
#define PLOG_INFO(fmt,...)      SPDLogger::print({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::info, fmt, ##__VA_ARGS__)
#define PLOG_WARN(fmt,...)      SPDLogger::print({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::warn, fmt, ##__VA_ARGS__)
#define PLOG_ERROR(fmt,...)     SPDLogger::print({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::err, fmt, ##__VA_ARGS__)
#define PLOG_FATAL(fmt,...)     SPDLogger::print({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::critical, fmt, ##__VA_ARGS__)

///< use like stream, e.g. SLOG_INFO() << "warn log: " << 1
#define SLOG_TRACE()            SPDLogger::logStream({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::trace)
#define SLOG_DEBUG()            SPDLogger::logStream({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::debug)
#define SLOG_INFO()             SPDLogger::logStream({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::info)
#define SLOG_WARN()             SPDLogger::logStream({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::warn)
#define SLOG_ERROR()            SPDLogger::logStream({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::err)
#define SLOG_FATAL()            SPDLogger::logStream({__FILE__, __LINE__, __FUNCTION__}, spdlog::level::critical)
```

## 2. 配置文件

为了便于使用者应用，`simpostOS`将日志系统的常见配置独立为`logger.yaml`配置文件，主要包含如下配置：

- `log_level`: 日志输出等级，低于该等级的日志不会输出；可选的等级有`trace`、`debug`、`info`、`warn`、`error`、`fatal`，默认等级为`info`；
- `log_file_number`: 日志文件转存个数，与`log_file_size`一起使用，默认配置为5；
- `log_file_size`: 日志文件大小，当日志文件达到限定大小后，会触发日志转存，转存个数到达限定个数后，会自动删除最老的日志文件；可识别的单位有`KB`、`MB`和`GB`，默认日志大小为`1024KB`；
- `log_to_console`: 日志是否输出到控制台，默认`false`不输出；
- `log_save_path`: 日志文件存放路径，指向可读写的日志分区，通过所有应用的日志文件大小和转存个数，可以得到日志分区的最小要求；
- `log_format`: 日志输出格式配置，推荐配置为`[%Y-%m-%d %H:%M:%S.%e] [%^-%L-%$] [%s:%# %!()] : %v`；

详见`logger.yaml`文件。
