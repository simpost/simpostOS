# 开始之前 #

在正式开始之前，首先熟悉一下`simpostOS`的三个使用规则：制品规则、命令行参数规则和配置文件。

## 一、制品规则

### 1.1 目录结构规则

为了规范可执行应用程序结构，同时简化业务的部署，`simpostOS`参照`Linux`系统目录结构，对应用程序制品目录结构做如下规范性要求。

1. **多服务目录结构**

针对产品的所有应用程序，可以统一放在相同的目录中，比如`/opt/simpost_services`目录：

```
$ pwd
/opt/simpost_services
$ tree .
.
├── upgrade
├── timesync
├── sysManager
├── devManager
└── netManager
```

2. **服务内目录结构**

每个服务内的目录，遵循如下结构规则：

```
service           # 服务名称【必选】
├── docs          # 存放设计文档、接口文档、使用手册等【可选】
├── include       # 存放应用服务二次开发的头文件【可选】
├── bin           # 存放应用服务可执行程序【必选】
├── lib           # 存放应用服务私有静态库与动态库【可选】
├── tools         # 存放工具可执行程序【可选】
├── plugins       # 存放应用服务时用到的插件【可选】
├── etc           # 存放应用服务配置文件【可选】
├── conf          # 存放应用服务配置文件【可选】
├── config        # 存放应用服务配置文件【可选】
├── data          # 存放应用程序用到的数学模型等【可选】
├── setup.sh      # 环境配置脚本，可用于手动运行【必选】
├── stop.sh       # 停止应用服务脚本【必选】
├── start.sh      # 启动应用服务脚本【必选】
├── status.sh     # 查看应用服务状态脚本【必选】
└── version.yaml  # 应用服务版本信息和依赖描述【必选】
```

其中：
1. `etc`、`conf`和`config`用户可自行选择用那个，默认使用`config`；
2. `docs`和`include`主要用于插件程序的二次开发，产品集成时可删除以节约存储空间：
3. `lib`目录存放应用程序特有的动态库，多个应用程序共用的动态库可放在公共依赖路径，以节约存储空间；
4. `version.yaml`文件用于记录应用程序的构建和版本信息，推荐在构建时自动生成，格式可以自定义，下面是一个示例：

```
name: simpostOS        # 服务名称
version: V0.4.0.0      # 服务版本
branch: develop        # 构建分支
commit: 0eca485        # 代码状态

image_tag: v2          # 构建镜像版本
image_name: simpost    # 构建镜像名称
build_type: Release    # 构建类型
build_time: 2024-04-16 15:05:07  # 构建事件
```

当然，也推荐在程序启动时将上面信息打印出来，便于在确定的代码状态下分析问题。

### 1.2 制品命名规则

将如上服务目录打包为`tar.gz`或`tar.bz2`格式存储，以确保链接文件的正确存储，同时文件命名满足如下规则：

> <serviceName>-<Vx.x.x.x>-<buildTime><.platform><.Debug/Release>.tar.gz

比如libev的三方组件，针对不同平台构建后的二进制包名为：

```
libev-V4.33.0-20230404203414.LC1881.Debug.tar.gz
libev-V4.33.0-20230404203414.SM8475.Debug.tar.gz
libev-V4.33.0-20230404203414.SDM660.Debug.tar.gz
libev-V4.33.0-20230404203414.LC1881.Release.tar.gz
libev-V4.33.0-20230404203414.SM8475.Release.tar.gz
libev-V4.33.0-20230404203414.SDM660.Release.tar.gz
```

## 二、命令行参数

`simpostOS`集成了`gflags`命令行参数解析工具，支持从环境变量、配置文件和命令行三种方式获取参数。

### 2.1 命令行传递参数

格式如下：

```
$ executable --参数1=值1 --参数2=值2 . . .
```

若输入了不识别的参数，程序会报错：

```
$ executable --参数=值1
ERROR: unknown command line flag `参数`
```

也可以使用`--nodefok`参数，可直指定当命令行中出现没有定义的参数时，并不退出（`error-exit`）。

### 2.2 配置文件传递参数

如果参数比较多，那么在执行程序的时候，会显得非常的冗余。可以使用`--flagfile`来指定从文件中读取参数值。比如`--flagfile=my.conf`表明要从`my.conf`文件读取参数的值。

```
--host=192.168.1.100
--port=10067
--fromenv=foo, bar
```

### 2.3 环境便利传递参数

如果我们想让程序直接从环境变量中读取参数值，可以使用`--formenv`来指定。比如：`--fromenv=foo,bar`表明要从环境变量读取`foo`，`bar`两个参数的值。

通过`export FLAGS_foo=xxx`和`export FLAGS_bar=yyy`程序就可读到`foo`，`bar`的值分别为`xxx`，`yyy`。

参数`--tryfromenv`与`--fromenv`类似，当参数的没有在环境变量定义时，不退出（`fatal-exit`）。

## 三、配置文件

正如前面介绍，`simpostOS`支持如下命令行参数：

- `plugin_dirname`：指定插件存放目录的名称，默认`plugins`；
- `config_dirname`：指定配置文件存放目录的名称，比如`etc`、`conf`、`config`，默认`config`；
- `log_profile`：指定日志配置文件名称，日志配置文件需要放在`config_dirname`指定的配置目录下；
- `deploy_profile`：指定部署文件名称，部署文件需要放在`config_dirname`指定的配置目录下。

注意：

- `simpostOS`就会自动加载`deploy_profile`指定的部署配置文件；
- 若使用`LOG_TYPE_FILE`参数调用日志接口时，`simpostOS`就会自动加载`log_profile`指定的日志配置文件：

> app.logInit(simpost::LOG_TYPE_FILE)
