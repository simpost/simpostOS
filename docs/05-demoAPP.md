# 第一个应用程序 #

## 一、程序源码

在`helloworld`的基础上，完成程序如下，增加常用接口调用：

```
#include <thread>
#include "simpost_application.hpp"

static void helloworld_routine(void)
{
    int32_t count = 0;
    if(false == simpost::Application::isInit())
    {
        LOG_ERROR("framework has not been init!");
        return ;
    }

    LOG_INFO("version: {}", simpost::Application::version());
    LOG_INFO("processname: {}", simpost::Application::processname());
    LOG_INFO("workspace: {}", simpost::Application::workspace());

    while (true)
    {
        if(count++ > 10)
            break;
        std::this_thread::sleep_for(std::chrono::seconds(2));
        LOG_INFO("Hello, world!");
    }
    simpost::Application::terminate();
}

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    std::thread helloworld(helloworld_routine);
    helloworld.detach();
    ///< 3. application run
    return app.exec();
}
```

## 二、新增接口介绍

新增接口的功能描述：
1. 第7行：`Application::isInit()`是全局函数，用于判断应用初始化是否成功；
2. 第13行：`Application::version()`是全局函数，用于获取`Application`的版本信息；
3. 第14行：`Application::processname()`是全局函数，用于获取当前进程的程序名称；
4. 第15行：`Application::workspace()`是全局函数，用于获取当前程序执行的绝对路径；
5. 第25行：`Application::terminate()`是全局函数，用于结束整个`Application`程序。

## 三、运行示例程序

构成程序，然后运行，终端输出如下：

```
$ source setup.sh
$ ./bin/demoAPP
[2024-04-16 12:32:27.927] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 12:32:27.927] [-I-] [simpost_application.cxx:54 log()] : demoAPP version: V1.0.0.0
[2024-04-16 12:32:27.927] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 12:32:27.927] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: 8ad03f4
[2024-04-16 12:32:27.927] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 12:32:27.927] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 12:32:27.927] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 12:32:27.927] [-I-] [main.cxx:13 helloworld_routine()] : version: V0.4.0.0
[2024-04-16 12:32:27.927] [-I-] [main.cxx:14 helloworld_routine()] : processname: demoAPP
[2024-04-16 12:32:27.927] [-I-] [main.cxx:15 helloworld_routine()] : workspace: /home/konishi/simpostOS
[2024-04-16 12:32:29.928] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:31.928] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:33.928] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:35.928] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:37.929] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:39.929] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:41.929] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:43.929] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:45.930] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:47.930] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:49.930] [-I-] [main.cxx:22 helloworld_routine()] : Hello, world!
[2024-04-16 12:32:49.930] [-I-] [erpc_threadpool.hpp:150 threadRoutine()] : ThreadPool: static routine will exit.
[2024-04-16 12:32:49.930] [-I-] [erpc_threadpool.hpp:150 threadRoutine()] : ThreadPool: static routine will exit.
[2024-04-16 12:32:49.930] [-I-] [erpc_threadpool.hpp:150 threadRoutine()] : ThreadPool: static routine will exit.
```