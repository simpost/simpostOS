# 通用定时器 #

本示例程序基于前面组件式编程基础上演变而来。

## 一、实现主程序

与组件示例主程序相同：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

## 二、在组件中调用定时器接口

使用通用定时器，只需要使用如下两个接口即可：

```
#include "erpc/erpc.h"

using erpc_timer_t = void *;
using erpc_timer_handler_t = std::function<void(erpc_timer_t, void *)>;

extern erpc_timer_t erpc_timer_start(uint64_t timeout_msec, bool reload, erpc_timer_handler_t handler, void *arg);
extern void erpc_timer_stop(erpc_timer_t handle);
```

- `erpc_timer_start()`用于启动一个定时器，可以所循环定时器或者一次定时器；
- `erpc_io_unregister()`用于停止一个定时器。

下面是一个循环调用四次的定时器示例程序：

```
#include "erpc/erpc.h"
#include "simpost_component.hpp"

class componentTimer: public simpost::Component
{
public:
    explicit componentTimer() : Component("moduleA", "V0.1.0") {}
    virtual ~componentTimer() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void timerHandler(erpc::erpc_timer_t handle, void *arg);
private:
    erpc::erpc_timer_t m_timer_ctx;
};

bool componentTimer::init(void)
{
    LOG_INFO("componentTimer init");
    m_timer_ctx = erpc::erpc_timer_start(3000, true, std::bind(&componentTimer::timerHandler, this, std::placeholders::_1, std::placeholders::_2), nullptr);
    return true;
}

void componentTimer::exit(void)
{
    LOG_INFO("componentTimer exit");
}

void componentTimer::timerHandler(erpc::erpc_timer_t handle, void *arg)
{
    static int timer_count = 0;
    if(timer_count++ > 3)
    {
        LOG_INFO("cycle timer stop");
        erpc::erpc_timer_stop(handle);
        return ;
    }
    LOG_INFO("cycle timer running...");
}

COMPONENT_REGISTER(componentTimer);
```

上面程序在初始化时，启动了一个循环定时器，并在循环定时器中计数四次后结束定时器，运行效果如下：

```
$ ./bin/demoTimer 
[2024-04-19 08:54:51.796] [-I-] [simpost_application.cxx:54 log()] : ##############################################################################
[2024-04-19 08:54:51.796] [-I-] [simpost_application.cxx:55 log()] : demoTimer-V0.4.0.0
[2024-04-19 08:54:51.796] [-I-] [simpost_application.cxx:56 log()] : workspace: /home/konishi/simpostOS
[2024-04-19 08:54:51.796] [-I-] [simpost_application.cxx:58 log()] : code state: develop-b01a810
[2024-04-19 08:54:51.796] [-I-] [componentTimer.cxx:19 init()] : componentTimer init
[2024-04-19 08:54:51.796] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-19 08:54:51.796] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing

[2024-04-19 08:54:54.799] [-I-] [componentTimer.cxx:38 timerHandler()] : cycle timer running...
[2024-04-19 08:54:57.802] [-I-] [componentTimer.cxx:38 timerHandler()] : cycle timer running...
[2024-04-19 08:55:00.805] [-I-] [componentTimer.cxx:38 timerHandler()] : cycle timer running...
[2024-04-19 08:55:03.808] [-I-] [componentTimer.cxx:38 timerHandler()] : cycle timer running...
[2024-04-19 08:55:06.811] [-I-] [componentTimer.cxx:34 timerHandler()] : cycle timer stop
```
