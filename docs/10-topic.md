# 发布者与订阅者示例 #

本示例程序基于前面组件式编程基础上演变而来。

## 一、实现主程序

与服务示例的主程序相同：

```
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

## 二、实现发布者程序

创建发布者程序的流程如下：

1. 参照组件编程流程，实现组件的正确注册与注销；
1. 包含`erpc/erpc.h`和`module/module_identity.pb.h`头文件;
2. 使用`erpc::erpc_topic_create()`方法创建主题，并指定获取主题的回调函数；
3. 根据业务特性，调用`erpc::erpc_topic_publish()`方法发布主题数据。

下面是`publisher.cxx`源码：

```
#include <thread>
#include <functional>
#include "erpc/erpc.h"
#include "simpost_component.hpp"
#include "module/module_name.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class publishAPP: public simpost::Component
{
public:
    explicit publishAPP() : Component("publisher", "V0.1.0") {}
    virtual ~publishAPP() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void publisherApp(void);
    void topicDataGet(google::protobuf::Any &args);
private:
    int m_private_data;
};

bool publishAPP::init(void)
{
    LOG_INFO("publishAPP init");
    m_private_data = 1;
    erpc::erpc_topic_create(MODULE_ID_ACCESS, 15, std::bind(&publishAPP::topicDataGet, this, std::placeholders::_1));
    std::thread app([this]{ publisherApp(); });
    app.detach();
    return true;
}

void publishAPP::exit(void)
{
    LOG_INFO("publishAPP exit");
}

void publishAPP::publisherApp(void)
{
    google::protobuf::Any args;
    google::protobuf::Int32Value value;
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        value.set_value(m_private_data);
        args.PackFrom(value);
        erpc::erpc_topic_publish(MODULE_ID_ACCESS, 15, args);
        m_private_data++;
    }
}

void publishAPP::topicDataGet(google::protobuf::Any &args)
{
    google::protobuf::Int32Value value;
    value.set_value(m_private_data);
    args.PackFrom(value);
}

COMPONENT_REGISTER(publishAPP);
```

## 三、实现订阅者程序

创建订阅者程序的流程如下：

1. 参照组件编程流程，实现组件的正确注册与注销；
2. 包含`erpc/erpc.h`和`module/module_identity.pb.h`头文件；
3. 使用`erpc::erpc_topic_subscribe()`方法订阅主题，并指定主题数据回调函数；
4. 根据业务需求，调用`erpc::erpc_topic_fetch()`方法，实时获取主题数据。

下面是`subscriber.cxx`源码：

```
#include <thread>
#include "erpc/erpc.h"
#include "simpost_component.hpp"
#include "module/module_name.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class subscribAPP: public simpost::Component
{
public:
    explicit subscribAPP() : Component("subscriber", "V0.1.0") {}
    virtual ~subscribAPP() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void subscriberAPP(void);
    void topicDataHandler(const google::protobuf::Any &args);
};

bool subscribAPP::init(void)
{
    LOG_INFO("subscribAPP init");
    std::thread app([this]{ subscriberAPP(); });
    app.detach();
    return true;
}

void subscribAPP::exit(void)
{
    LOG_INFO("subscribAPP exit");
}

void subscribAPP::subscriberAPP(void)
{
    google::protobuf::Any args;
    google::protobuf::Int32Value value;
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    erpc::erpc_topic_subscribe("publisher", MODULE_ID_ACCESS, MODULE_ID_ACCESS, 15, std::bind(&subscribAPP::topicDataHandler, this, std::placeholders::_1));
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(3));
        if(false == erpc::erpc_topic_fetch("publisher", MODULE_ID_ACCESS, 15, args, 100))
        {
            LOG_ERROR("fetch 'publishAPP/15' data failed!");
            continue;
        }
        args.UnpackTo(&value);
        LOG_INFO("fetch topic 'publishAPP/15' data: {}", value.value());
    }
}

void subscribAPP::topicDataHandler(const google::protobuf::Any &args)
{
    google::protobuf::Int32Value value;
    if(!args.UnpackTo(&value))
    {
        LOG_ERROR("unpack 'publishAPP/15' data failed!");
        return;
    }
    LOG_INFO("recv topic 'publishAPP/15' data: {}", value.value());
}

COMPONENT_REGISTER(subscribAPP);
```

## 四、运行示例程序

拷贝部署配置文件，并运行示例程序：

```
$ source setup.sh
$ cp example/07-topic/deployment.yaml config/
$ ./bin/demoTopic
[2024-04-16 14:59:09.672] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 14:59:09.672] [-I-] [simpost_application.cxx:54 log()] : demoTopic version: V1.0.0.0
[2024-04-16 14:59:09.672] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 14:59:09.672] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:59:09.673] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:59:09.673] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:178 parseBasicConfig()] : basicConfig:
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:179 parseBasicConfig()] :  - uid: publisher
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:180 parseBasicConfig()] :  - listenPort: 17011
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:181 parseBasicConfig()] :  - message debug: true
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:182 parseBasicConfig()] : - default time: 5, cycle: 3000
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:183 parseBasicConfig()] :  - bufferSize: 4096, messageSize: 100
[2024-04-16 14:59:09.673] [-I-] [erpc_profile.cxx:112 parseBasicConfig()] : source mask: 127.0.0.1/255.255.255.0
[2024-04-16 14:59:09.673] [-W-] [erpc_profile.cxx:137 parseNetConfig()] : warn config: no dependService!
[2024-04-16 14:59:09.673] [-I-] [erpc_transmit.cxx:179 erpc_transmit_init()] : transmit buffer size: 4096
[2024-04-16 14:59:09.673] [-I-] [subscriber.cxx:22 init()] : subscribAPP init
[2024-04-16 14:59:09.673] [-I-] [publisher.cxx:25 init()] : publishAPP init
[2024-04-16 14:59:09.673] [-I-] [erpc_router.hpp:155 routine()] : message router is runing

[2024-04-16 14:59:11.673] [-I-] [subscriber.cxx:60 topicDataHandler()] : recv topic 'publishAPP/15' data: 1
[2024-04-16 14:59:12.694] [-I-] [subscriber.cxx:48 subscriberAPP()] : fetch topic 'publishAPP/15' data: 2
[2024-04-16 14:59:13.674] [-I-] [subscriber.cxx:60 topicDataHandler()] : recv topic 'publishAPP/15' data: 2
[2024-04-16 14:59:15.674] [-I-] [subscriber.cxx:60 topicDataHandler()] : recv topic 'publishAPP/15' data: 3
[2024-04-16 14:59:15.695] [-I-] [subscriber.cxx:48 subscriberAPP()] : fetch topic 'publishAPP/15' data: 4
[2024-04-16 14:59:17.674] [-I-] [subscriber.cxx:60 topicDataHandler()] : recv topic 'publishAPP/15' data: 4
[2024-04-16 14:59:18.695] [-I-] [subscriber.cxx:48 subscriberAPP()] : fetch topic 'publishAPP/15' data: 5
[2024-04-16 14:59:19.674] [-I-] [subscriber.cxx:60 topicDataHandler()] : recv topic 'publishAPP/15' data: 5
```
