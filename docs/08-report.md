# 上报单向通信 #

本示例程序基于前面组件式编程基础上演变而来。

## 一、实现主程序

相比签名组件式编程，增加组件模块间业务交互的需求，增加部署初始化的调用：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

## 二、实现服务组件程序

创建服务组件程序流程如下：

1. 参照组件编程流程，实现组件的正确注册与注销；
2. 包含`erpc/erpc.h`和`module/module_identity.pb.h`头文件；
3. 在`init()`函数中，使用`erpc::erpc_report_subscribe()`注册回调函数，以处理关注的上报事件；
4. 在`exit()`函数中，使用`erpc::erpc_report_unsubscribe()`注销上报事件。

下面是一个服务组件程序的完整源码：

```
#include "erpc/erpc.h"
#include "simpost_component.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class server: public simpost::Component
{
public:
    explicit server() : Component("server", "V0.1.0") {}
    virtual ~server() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void report(const google::protobuf::Any &args);
};

bool server::init(void)
{
    LOG_INFO("server init");
    erpc::erpc_report_subscribe(simpost::protocol::MODULE_ID::MODULE_ID_POWER, 10, 
                                std::bind(&server::report, this, std::placeholders::_1));
    return true;
}

void server::exit(void)
{
    erpc::erpc_report_unsubscribe(simpost::protocol::MODULE_ID::MODULE_ID_POWER, 10);
    LOG_INFO("server exit");
}

void server::report(const google::protobuf::Any &args)
{
    google::protobuf::Int32Value value;
    args.UnpackTo(&value);
    LOG_INFO("receive report data: {}", value.value());
}

COMPONENT_REGISTER(server);
```

## 三、实现客户组件程序

创建客户组件程序流程如下：

1. 参照组件编程流程，实现组件的正确注册与注销；
2. 包含`erpc/erpc.h`和`module/module_identity.pb.h`头文件；
3. 根据业务需要，使用`erpc::erpc_report_publish()`方法定向的向远端上报数据信息。

下面是一个客户组件程序的完整源码：

```
#include <thread>
#include "erpc/erpc.h"
#include "simpost_component.hpp"
#include "module/module_identity.pb.h"
#include "google/protobuf/wrappers.pb.h"

class reporter: public simpost::Component
{
public:
    explicit reporter() : Component("reporter", "V0.1.0") {}
    virtual ~reporter() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void reportRoutine(void);
};

bool reporter::init(void)
{
    LOG_INFO("reporter init");
    std::thread reporter([this]{ reportRoutine(); });
    reporter.detach();
    return true;
}

void reporter::exit(void)
{
    LOG_INFO("reporter exit");
}

void reporter::reportRoutine(void)
{
    int32_t count = 1;
    google::protobuf::Any args;
    google::protobuf::Int32Value value;
    while (true)
    {
        value.set_value(count++);
        args.PackFrom(value);
        std::this_thread::sleep_for(std::chrono::seconds(2));
        erpc::erpc_report_publish("service", simpost::protocol::MODULE_ID::MODULE_ID_POWER, 10, args);
    }
}

COMPONENT_REGISTER(reporter);
```

在该示例中，每隔2S向服务端定时上报计数数据。

## 四、运行示例程序

此示例程序实现了两个不同组件之间的单向业务交互，也是最简单的业务交互。业务交互依赖于一个部署配置文件`config/deployment.yaml`，运行方法如下：

```
$ source setup.sh
$ cp example/05-report/deployment.yaml config/
$ ./bin/reporter
[2024-04-16 14:08:41.452] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 14:08:41.452] [-I-] [simpost_application.cxx:54 log()] : reporter version: V0.4.0.0
[2024-04-16 14:08:41.452] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 14:08:41.452] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: ed52f1b
[2024-04-16 14:08:41.452] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:08:41.452] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:08:41.452] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 14:08:41.453] [-I-] [erpc_profile.cxx:110 basicConfig()] : basicConfig: service
[2024-04-16 14:08:41.453] [-I-] [erpc_profile.cxx:111 basicConfig()] : listenPort: 17011, bufferSize: 4096, messageSize: 100
[2024-04-16 14:08:41.453] [-I-] [erpc_profile.cxx:112 basicConfig()] : source mask: 127.0.0.1/255.255.255.0
[2024-04-16 14:08:41.453] [-W-] [erpc_profile.cxx:137 parseNetConfig()] : warn config: no dependService!
[2024-04-16 14:08:41.453] [-I-] [erpc_transmit.cxx:179 erpc_transmit_init()] : transmit buffer size: 4096
[2024-04-16 14:08:41.453] [-I-] [server.cxx:19 init()] : server init
[2024-04-16 14:08:41.453] [-I-] [report.cxx:20 init()] : reporter init
[2024-04-16 14:08:41.453] [-I-] [erpc_router.hpp:155 routine()] : message router is runing
[2024-04-16 14:08:43.454] [-I-] [server.cxx:35 reportHandler()] : receive report data: 1
[2024-04-16 14:08:45.454] [-I-] [server.cxx:35 reportHandler()] : receive report data: 2
[2024-04-16 14:08:47.454] [-I-] [server.cxx:35 reportHandler()] : receive report data: 3
...
```

