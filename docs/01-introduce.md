# `simpostOS`介绍 #

## 一、`simpostOS`简介

产品公司的生存之道是：**找到各项技术的最优排列组合解，从而以更低成本、更高效率的研发出更高品质和更好体验的产品，从而获得更高的产品势能，最终结合营销和渠道，在商业上获得成功**。

`simpostOS`刚好契合这一生存之道。因为`simpostOS`是**一套设计方法**，以及**多个软件组件、服务的集合**。`simpostOS`能让智能硬件产品的软件和硬件模块化，让产品构建实现高复用的低成本，搭积木似的高效率。同时，模块的复用性进一步反推技术的成熟度、稳定性，还可解放研发资源投入到产品的更高品质、更好体验上。

## 二、`simpostOS`核心目标

**通过技术模块化实现智能硬件产品更高效的研发过程，在提供更高质量、更高品质产品的同时，降低研发投入的成本，让智能硬件产品公司将重心放在用户需求挖掘和更优秀的产品定义上**。

## 三、`simpostOS`设计理念

1. 将`simpostOS`与硬件解耦，不挑芯片，实现芯片异构；
2. 将`simpostOS`与系统解耦，不挑操作系统，实现系统异构；
3. 将`simpostOS`与语言解耦，不挑编程语言，实现语言异构；
4. 通用能力`CBB`化，组件复用率可达100%，服务复用率可达75%以上；
5. 软件编程遵循：功能代码与业务代码分离，业务代码与数据处理代码分离；
6. 向上提供`service`、`report`和`sub/pub`接口，满足各种应用业务场景的需求；
7. 应用只关需注模块与模块之间的业务交互，而忽略线程间、进程间和芯片间的通信，并且可支持网络、`USB`、`UART`、蓝牙、`zigbee`等连接方式；

下面简单介绍一下，`simpostOS`是如何实现如上设计理念的。

## 四、将软件与操作系统和硬件分离

![image](./images/system_block.png)

系统层面：
1. 通过操作系统接口的抽象（`OSAL`），屏蔽`Android`、`Linux`和`RTOS`的系统差异，从而做到操作系统分离；
2. 通过设备能力抽象（`DevAL`），屏蔽`USB`、`UART`、`SPI`、`IIC`等总线差异，同时还屏蔽不同硬件接口差异，向应用层提供统一的设备能力和数据接口，从而做到软件与硬件的分离。

协议层面：`simpostOS`底层使用`PB(Google protobuf)`设计进程间、芯片间的交互协议，不同的业务可以根据业务特性和企业现状选择不同的编程语言实现。

【备注】：在特殊的场景下，可以将与传感器关联紧密的算法放在与硬件驱动层实现，以提供更好的响应速度、更好的业务抽象和更好的用户体验。

## 五、通用能力`CBB`化

采用货架式管理办法，将通用组件、通用服务能力统一进行管理，货架不仅可以管理当前组件和服务，还会记录全链路的依赖关系，自动生成类似下图所示的依赖图：

![image](./images/depends.png)

在最终产品集成时，可以很好的将服务集成在各自的目录下，而所有服务依赖的通用组件只集成一份放在统一的目录下，从而极大的增强了系统存储的利用率。

同时，通过组件和服务能力的`CBB`化，可以实现组件复用率可达100%，服务复用率可达75%以上。这极大的降低了产品研发的重复投入，不仅大大提升了研发效率，还保障了稳定的产品质量。

## 六、消息通信中间件`erpc`

`erpc`消息通信中间件，是基于优秀的`protobuf`进行协议设计，向下屏蔽了网络、`USB`、`UART`、蓝牙、`zigbee`等通信方式，向上提供`service`、`report`、`sub/pub`接口调用，满足各种应用业务场景的需求的同时，隐藏了跨线程、跨进程和跨芯片的复杂交互过程。

![image](./images/erpc.png)

同时，`erpc`底层协议采用`protobuf`，天然支持**多语言的异构编程**，非常适用于各种边缘与终端的智能产品使用。

## 七、应用编程框架`simpCore`

为了简化应用程序编程的难度，`simpCore`在`erpc`的基础上，将参数解析方法、配置文件、日志文件和接口、程序崩溃捕获方法等融合成一个整体，对外提供`Application`类、`Components`类和`pluginWrapper`类，实现了`组件化`、`插件化`应用编程。

### 7.1 基于`Application`类编写程序

只需要定义`simpost::Application`对象`app`，并调用`app.exec()`方法，即可完成应用程序的开发：

```
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. application run
    return app.exec();
}
```

### 7.2 基于`Components`类编写组件

只需要继承`simpost::Component`类，实现`init()`和`exit()`方法，并调用`COMPONENT_REGISTER()`将类注册进组件中，程序运行时即可将组件调用起来。

```
#include "simpost_component.hpp"

class demoTool: public simpost::Component
{
public:
    explicit demoTool() : Component("demoTool", "V0.1.0") {}
    virtual ~demoTool() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool demoTool::init(void)
{
    LOG_INFO("tool init");
    LOG_INFO("workspace: {}", simpost::Application::workspace());
    return false;
}

void demoTool::exit(void)
{
    LOG_INFO("tool exit");
}

COMPONENT_REGISTER(demoTool);
```

### 7.3 基于`Components`类编写插件

只需要继承`simpost::Component`类，实现`init()`和`exit()`方法，并调用`PLUGIN_REGISTER()`注册，将组件编译为编译为动态库`*.so`插件，程序运行时就会动态加载`plugins`目录下所有的插件。

```
#include "simpost_component.hpp"

class componentA: public simpost::Component
{
public:
    explicit componentA() : Component("plugin", "V0.1.0") {}
    virtual ~componentA() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool componentA::init(void)
{
    LOG_INFO("componentA plugin init");
    return true;
}

void componentA::exit(void)
{
    LOG_INFO("componentA plugin exit");
}

PLUGIN_REGISTER(componentA);
```

组件化编程的实现，可以使得智能硬件产品的功能开发与实际运行部署分开，实现一次构建，多种部署环境的运行成为可能。

## 八、`simpostOS`的特点

- 支持参数解析，运行更加灵活
- 支持`rpc`调用，指令实现更加简单
- 支持`report`调用，定向通信更节省
- 支持`sub/pub`方法，复杂业务更易实现
- 协议采用`protobuf`，天然支持向前兼容
- 协议采用`protobuf`，天然支持异构编程
- 集成`backtrace`方法，支持崩溃回溯定位
- 组件式应用程序编程框架，让程序编程更加灵活
- 插件式应用程序运行框架，让动态部署成为可能
- 支持事件驱动状态机框架，让复杂业务更加有序
- 支持物模型（TSL）数据，让物联网接入更平滑
- 集成`yaml`格式的配置文件，可一次构建多次运行
- 集成`spdlog`的日志库，提供`fmt`、`print`和`stream`三种日志接口
- 系统与硬件抽象，支持`Android`、`Linux`和各种`RTOS`
- 支持模块间的文件传输和数据传输方法，数据与文件访问更安全

## 七、基于`simpostOS`实现的通用服务

1. `timeSyncService`：时间同步服务，对外提供`NTP`时间同步，和系统`clock`时间同步，可实现高精度的同步应用处理；
2. `upgadeService`：升级服务，包含升级服务端、升级客户端和升级节点三类服务，配合`msgProxy`，可实现不修改升级服务代码的情况下扩展升级模块；并且支持各种安全加密，保障升级安全；
3. `msgProxy`：消息代理，可实现跨产品间的业务交互，消息静态与动态路由，内外网络隔离等；
4. `uartMsgProxy`：串口通信消息代理，可实现基于串口的流式消息通信消息路由，可满足串口连接与并行连接的各种场景需求，可配合`msgProxy`使用；
5. `tcpMsgProxy`：`TCP`通信消息代理，与`uartMsgProxy`类似，只是专用于`TCP`通信的数据流通信，也可满足串行连接与并行连接的各种场景需求（`TCP`正常没有串行连接的场景），可配合`msgProxy`使用；
6. `udpProxy`：`UDP`代理，实现定向的`UDP`数据流转发，不含消息处理，可用于网络跳转；
7. `tcpProxy`：`TCP`代理，实现定向的`TCP`数据流转发，不含消息处理，可用于网络跳转；
8. `rosGateway`：`ROS2`协议网关，在`ROS`消息定义合理的情形下，只需要通过配置文件，即可完成`ROS2`消息与`PB`消息的互联互通；
9. 支持用户自定义插件接口，可适用于软硬件隔离、数据与业务隔离、算法与业务隔离等多种场景；
10. 支持`license`功能，可实现产品的在线授权与离线授权两种形式，并可做到一机一密授权；
11. 支持`Capability`能力集，配合产品型号与版本号，可实现`HMI`的无代码编程框架的最大化应用。

## 七、`simpostOS`后期规划

1. 实现物模型识别与转换模块，可接入华为、小米、`TCL`、`Matter`等生态；
2. 实现系统管理服务、日志收集服务、埋点服务等通用服务，让产品聚焦卖点功能的研发。
