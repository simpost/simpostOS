# 你好，世界 #

## 一、程序源码

基于`simpostOS`编写的`helloworld`程序如下：

```
#include <thread>
#include "simpost_application.hpp"

static void helloworld_routine(void)
{
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        LOG_INFO("Hello, world!");
    }
}

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    std::thread helloworld(helloworld_routine);
    helloworld.detach();
    ///< 3. application run
    return app.exec();
}
```

## 二、程序详解

1. 第2行：使用`simpostOS`编写应用程序，只需要包含一个头文件：`simpost_application.hpp`；
2. 第4~11行：业务线程，每个2秒打印`Hello, world!`；
3. 第15行：使用`Application`定义应用对象`app`，传入参数和应用版本信息；
4. 第17行：初始化日志，可选终端日志`LOG_TYPE_CONSOLE`和文件日志`LOG_TYPE_FILE`两种；日志文件依赖于日志配置文件，且使用大小方式转存；可传入代码分支`branch`和代码提交`commit`，程序启动日志会输出，便于二进制程序与代码的快速对应；
5. 第20行：`Application`框架初始化，可指定线程池的个数和最大的任务数量；
6. 第22~23行：创建业务线程，线程的作用是打印`Hello, world!`；
7. 第25行：运行`Application`框架，开始完整业务调度。

## 三、运行程序

构建程序，然后运行，终端输出如下：

```
$ source setup.sh
$ ./bin/helloworld
[2024-04-16 12:16:52.550] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 12:16:52.550] [-I-] [simpost_application.cxx:54 log()] : helloworld version: V1.0.0.0
[2024-04-16 12:16:52.550] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 12:16:52.551] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 12:16:52.551] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 12:16:52.551] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing

[2024-04-16 12:16:54.551] [-I-] [main.cxx:9 helloworld_routine()] : Hello, world!
[2024-04-16 12:16:56.551] [-I-] [main.cxx:9 helloworld_routine()] : Hello, world!
[2024-04-16 12:16:58.551] [-I-] [main.cxx:9 helloworld_routine()] : Hello, world!
[2024-04-16 12:17:00.551] [-I-] [main.cxx:9 helloworld_routine()] : Hello, world!
[2024-04-16 12:17:02.552] [-I-] [main.cxx:9 helloworld_routine()] : Hello, world!
```