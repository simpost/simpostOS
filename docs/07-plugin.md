# 插件示例程序 #

## 一、插件式编程介绍

插件编程方法是在组件式编程的基础上演变而来，其实现框架与组件式编程完全一致，区别只有三点：

1. 主程序调用插件初始化代替组件初始：`app.pluginInit()`；
2. 组件源码调用插件注册代替组件注册：`PLUGIN_REGISTER(componentA)`；
3. 程序构建时，组件构建为独立的动态库，与可执行的主程序分别构建。

也即：**组件编程是构建时静态建立组件与进程的关联关系；插件编程是在运行时动态建立组件与进程的关联关系**。使用插件式编程方法，可以达到如下效果：

1. 插件动态库与应用进程分别独立构建，且应用进程只需一次构建；
2. 应用进程与业务解耦，可根据业务需求实现动态部署。

## 二、插件主程序源码

在组件式主程序基础上，替换`app.componentInit()`为`app.pluginInit()`即可：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. load plugin and init
    if(false == app.pluginInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

其中，`SERVICE_VERSION_STR`、`SERVICE_BRANCH_NAME`和`SERVICE_COMMIT_HASH`来源于`cmake`构建工具的自动化获取。

## 三、编写插件应用程序

插件应用程序编码流程如下（可对比组件编程流程）：

1. 包含头文件：`simpost_component.hpp`;
2. 继承`simpost::Component`定义组件的子类；
3. 复写`init()`和`exit()`函数，分别在进程启动与退出时调用；
4. 使用`PLUGIN_REGISTER()`将组件子类插件框架中；
5. 构建组件源码为动态库，由主程序在运行时动态加载。

注意：对于`init()`函数的返回值：若返回`false`，则程序会立即结束；若返回`true`，程序才进入事件驱动循环。

比如下面源码分别实现了两个插件应用：

- `pluginA.cxx`

```
#include "simpost_component.hpp"

class pluginA: public simpost::Component
{
public:
    explicit pluginA() : Component("plugin", "V0.1.0") {}
    virtual ~pluginA() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool pluginA::init(void)
{
    LOG_INFO("pluginA plugin init");
    return true;
}

void pluginA::exit(void)
{
    LOG_INFO("pluginA plugin exit");
}

PLUGIN_REGISTER(pluginA);
```

- `pluginB.cxx`

```
#include "simpost_component.hpp"

class pluginB: public simpost::Component
{
public:
    explicit pluginB() : Component("moduleB", "V0.1.0") {}
    virtual ~pluginB() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool pluginB::init(void)
{
    LOG_INFO("pluginB init");
    return true;
}

void pluginB::exit(void)
{
    LOG_INFO("pluginB exit");
}

PLUGIN_REGISTER(pluginB);
```

## 四、构建插件应用程序

插件应用程序需要将主程序与组件程序分别构建，且组件程序构建为动态库供主程序运行时加载。

```
///< 构建主程序
add_executable(loader loader.cxx)
target_link_libraries(loader PRIVATE vv protobuf erpc yaml-cpp uv)

///< 构建插件动态库
add_library(pluginA SHARED pluginA.cxx)
install(TARGETS pluginA LIBRARY DESTINATION plugins)

add_library(pluginB SHARED pluginB.cxx)
install(TARGETS pluginB LIBRARY DESTINATION plugins)
```

## 五、运行插件应用程序

只需要将要加载的插件放在工作路径的`plugins`目录，然后运行主程序即可：

```
$ source setup.sh
# 只放 pluginA.so 在 plugins 目录
$ ./bin/loader
[2024-04-16 13:39:23.391] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 13:39:23.391] [-I-] [simpost_application.cxx:54 log()] : loader version: V0.4.0.0
[2024-04-16 13:39:23.391] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 13:39:23.391] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: 3d273a2
[2024-04-16 13:39:23.391] [-I-] [simpost_application.cxx:85 pluginInit()] : Load plugins from /home/konishi/simpostOS/plugins
[2024-04-16 13:39:23.391] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:39:23.391] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:39:23.391] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:39:23.392] [-I-] [pluginA.cxx:14 init()] : pluginA init
[2024-04-16 13:39:23.392] [-I-] [simpost_application.cxx:124 pluginInit()] : Load plugin: moduleA-V0.1.0

# 只放 pluginB.so 在 plugins 目录
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:54 log()] : loader version: V0.4.0.0
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: 3d273a2
[2024-04-16 13:41:21.841] [-I-] [simpost_application.cxx:85 pluginInit()] : Load plugins from /home/konishi/simpostOS/plugins
[2024-04-16 13:41:21.841] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:41:21.841] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:41:21.841] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:41:21.841] [-I-] [pluginB.cxx:14 init()] : pluginB init
[2024-04-16 13:41:21.841] [-I-] [simpost_application.cxx:124 pluginInit()] : Load plugin: moduleB-V0.1.0

# 同时放 pluginA.so 与 pluginB.so 在plugins 目录
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:54 log()] : loader version: V0.4.0.0
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 13:41:21.840] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: 3d273a2
[2024-04-16 13:41:21.841] [-I-] [simpost_application.cxx:85 pluginInit()] : Load plugins from /home/konishi/simpostOS/plugins
[2024-04-16 13:41:21.841] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:41:21.841] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:41:21.841] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:41:21.841] [-I-] [pluginB.cxx:14 init()] : pluginB init
[2024-04-16 13:41:21.841] [-I-] [simpost_application.cxx:124 pluginInit()] : Load plugin: moduleB-V0.1.0
[2024-04-16 13:41:21.842] [-I-] [pluginA.cxx:14 init()] : pluginA init
[2024-04-16 13:41:21.842] [-I-] [simpost_application.cxx:124 pluginInit()] : Load plugin: moduleA-V0.1.0
```
