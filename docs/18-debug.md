# 调试方法 #

`simpostOS`目前提供了三种调试方法：

1. 协议交互调试；
2. 业务流程调试；
3. 程序崩溃栈回溯。

## 一、协议交互调试

参见[部署配置文件说明](./11-deployment.md)，将配置`msgDebug`设置为`true`即可。如果日志采用的是`LOG_TYPE_FILE`形式，还需要确保`logger.yaml`配置中的日志等级是`info`或以下级别。

重新运行`report`、`service`或`topic`示例程序，就可以看到完整的协议交互流程：

```
[2024-04-12 11:01:51.869] [-I-] [simpost_application.cxx:49 init()] : #####################################################################################
[2024-04-12 11:01:51.869] [-I-] [simpost_application.cxx:50 init()] : demoServer version: V1.0.0.0
[2024-04-12 11:01:51.869] [-I-] [simpost_application.cxx:51 init()] : workspace: /home/konishi/simpostOS
[2024-04-12 11:01:51.869] [-I-] [erpc_profile.cxx:106 basicConfig()] : basicConfig: service
[2024-04-12 11:01:51.870] [-I-] [erpc_profile.cxx:107 basicConfig()] : listenPort: 17011, bufferSize: 1024
[2024-04-12 11:01:51.870] [-I-] [erpc_profile.cxx:108 basicConfig()] : source mask: 127.0.0.1/
[2024-04-12 11:01:51.870] [-I-] [erpc_profile.cxx:148 parseNetConfig()] : dependService:
[2024-04-12 11:01:51.870] [-I-] [erpc_profile.cxx:178 parseNetConfig()] :  - application(demoAPP), 127.0.0.1:17010
[2024-04-12 11:01:51.870] [-I-] [erpc_transmit.cxx:179 erpc_transmit_init()] : transmit buffer size: 1024
[2024-04-12 11:01:51.870] [-I-] [erpc_router.hpp:146 routine()] : message router is runing
[2024-04-12 11:01:51.870] [-I-] [erpc_threadpool.hpp:133 threadRoutine()] : ThreadPool: static routine runing
[2024-04-12 11:01:51.870] [-I-] [erpc_threadpool.hpp:133 threadRoutine()] : ThreadPool: static routine runing
[2024-04-12 11:01:51.870] [-I-] [erpc_threadpool.hpp:133 threadRoutine()] : ThreadPool: static routine runing
[2024-04-12 11:01:51.870] [-I-] [demoServer.cxx:19 init()] : demoService init
[2024-04-12 11:02:09.175] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_13, seq_0
[2024-04-12 11:02:09.176] [-I-] [demoServer.cxx:40 testService()] : test service called: 3
[2024-04-12 11:02:09.179] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_13, seq_0
[2024-04-12 11:02:09.180] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_10, seq_1
[2024-04-12 11:02:09.180] [-I-] [demoServer.cxx:40 testService()] : test service called: 5
[2024-04-12 11:02:09.186] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_10, seq_1
[2024-04-12 11:02:09.186] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_15, seq_2
[2024-04-12 11:02:09.187] [-E-] [erpc_handler.cxx:67 erpc_request_handler()] : dones't have 'audio_15' service!
[2024-04-12 11:02:09.187] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_15, seq_2
[2024-04-12 11:02:12.188] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_13, seq_3
[2024-04-12 11:02:12.188] [-I-] [demoServer.cxx:40 testService()] : test service called: 3
[2024-04-12 11:02:12.191] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_13, seq_3
[2024-04-12 11:02:12.192] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_10, seq_4
[2024-04-12 11:02:12.192] [-I-] [demoServer.cxx:40 testService()] : test service called: 5
[2024-04-12 11:02:12.197] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_10, seq_4
[2024-04-12 11:02:12.198] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_15, seq_5
[2024-04-12 11:02:12.199] [-E-] [erpc_handler.cxx:67 erpc_request_handler()] : dones't have 'audio_15' service!
[2024-04-12 11:02:12.199] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_15, seq_5
[2024-04-12 11:02:15.200] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_13, seq_6
[2024-04-12 11:02:15.200] [-I-] [demoServer.cxx:40 testService()] : test service called: 3
[2024-04-12 11:02:15.203] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_13, seq_6
[2024-04-12 11:02:15.204] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_10, seq_7
[2024-04-12 11:02:15.204] [-I-] [demoServer.cxx:40 testService()] : test service called: 5
[2024-04-12 11:02:15.209] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_10, seq_7
[2024-04-12 11:02:15.210] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_15, seq_8
[2024-04-12 11:02:15.210] [-E-] [erpc_handler.cxx:67 erpc_request_handler()] : dones't have 'audio_15' service!
[2024-04-12 11:02:15.210] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_15, seq_8
[2024-04-12 11:02:18.211] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_13, seq_9
[2024-04-12 11:02:18.211] [-I-] [demoServer.cxx:40 testService()] : test service called: 3
[2024-04-12 11:02:18.214] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_13, seq_9
[2024-04-12 11:02:18.215] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_10, seq_10
[2024-04-12 11:02:18.215] [-I-] [demoServer.cxx:40 testService()] : test service called: 5
[2024-04-12 11:02:18.221] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_10, seq_10
[2024-04-12 11:02:18.221] [-I-] [erpc_driver.cxx:61 erpc_dispatch_message()] : <<<= request: application -> service, audio_15, seq_11
[2024-04-12 11:02:18.221] [-E-] [erpc_handler.cxx:67 erpc_request_handler()] : dones't have 'audio_15' service!
[2024-04-12 11:02:18.222] [-I-] [erpc_router.hpp:122 sendMessage()] : =>>> response: service -> application, audio_15, seq_11
```

如上面日志：

1. `=>>>`代表发送出去的消息，`<<<=`代表接收到的消息；
2. 协议交互消息的格式为：`<type>: <src_uid> -> <dst_uid>, <modulename>_<bid>, seq_<sid>`
    - `type`：消息类型，可能是`request`、`response`、`report`、`public`四种；
    - `src_uid`：消息发出者的唯一`uid`；
    - `dst_uid`：消息接收者的唯一`uid`；
    - `modulename`：业务交互的模块名；
    - `<bid>`：业务编号，可能是服务编号、上报编号或者主题编号；
    - `<sid>`：序列编号，每个消息都有唯一的序列号，注意统一服务的`request`与`response`的序列编号要相同。
3. 结合下面的业务流程调试方法，可以清晰的看到业务交互与业务流程之间的关系和流程。

## 二、业务流程调试

业务流程调试是基于`spd_logger.hpp`而来。可根据个人习惯，选择`print`、`stream`或`fmt`格式日志接口，并且严格遵循日志等级调用日志相关接口：
    - `TRACE`：追踪日志，用于记录程序详细执行信息，便于分析程序运行过程，比如函数调用、变量值等；
    - `DEBUG`：调试日志，用于记录程序执行信息，便于开发人员进行调试，比如内部状态、关键变量值等；
    - `INFO`：信息日志，用于记录程序的运行状态，便于后续分析和监控，比如程序启动、配置信息、状态变更等；
    - `WARN`：警告日志，用于记录可能存在潜在的问题或异常情况，但不会导致程序错误或崩溃，以便提醒开发人员或用户注意；
    - `ERROR`：错误日志，用于记录程序的异常情况或可恢复的错误信息，但不会导致程序崩溃，以便指导开发人员进行问题修复；
    - `FATAL`：致命日志，用于记录无法恢复的严重错误，需要立即处理。

然后根据程序的不同开发阶段，配置不同的日志等级：

1. 开发阶段：可根据软件成熟度配置`log_level`为`debug`或`info`；
2. 测试阶段：推荐配置`log_level`为`info`，通过问题反推开发人员完善`info`级别日志；
3. 生产阶段：配置`log_level`为`info`，确保生产/上线的产品，当出现异常能有足够的日志分析问题。

同时，在业务联调时，配合部署配置文件的`msgDebug`配置，可以详细的看到业务流程与交互流程，更利于问题的定位与分析。

## 三、程序崩溃栈回溯

在`simpostOS`中，提供了一个简易的程序崩溃栈回溯信息，只需要在`app.coreInit()`之后，调用`app.traceEnable()`使能栈回溯信息即可。当程序出现异常，栈回溯信息会记录在文件日志的末尾，以便快速查阅。

如果`Linux`系统配置了墓碑文件，应用程序就不用调用`app.traceEnable()`，而让系统监听崩溃事件（`SIGSEGV`、`SIGABRT`、`SIGFPE`、`SIGBUS`），并记录完整的内存信息，更易于问题的分析。
