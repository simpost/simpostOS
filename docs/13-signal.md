# 信号处理程序 #

本示例程序基于前面组件式编程基础上演变而来。

## 一、实现主程序

与组件示例主程序相同：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

## 二、主程序中添加信号处理

可以在主程序中，注册结束相关信号的处理方法：

```
#include "config.h"
#include "simpost_application.hpp"

static void terminateHandler(simpost::signal_t handle, int signo, void *arg)
{
    simpost::Application::componentExit();  ///< call componentInit() in main()
    simpost::Application::terminate();      ///< release all resource
}

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. register SIGINT or SIGQUIT handler
    if(false == app.signalRegister(SIGINT, terminateHandler, nullptr))
        return -1;
    ///< 4. component init
    if(false == app.componentInit())
        return -1;
    ///< 5. application run
    return app.exec();
}
```

上面增加了`void terminateHandler(simpost::signal_t handle, int signo, void *arg)`回调函数，和`app.signalRegister(SIGINT, terminateHandler, nullptr)`调用。

## 二、在组件中调用信号处理函数

要处理信号，只需要使用如下两个接口即可：

```
#include "erpc/erpc.h"

using erpc_signal_t = void *;
using erpc_signal_handler_t = std::function<void(erpc_signal_t, int, void *)>;

extern erpc_signal_t erpc_signal_register(int signo, erpc_signal_handler_t handler, void *arg);
extern void erpc_signal_unregister(erpc_signal_t handle);
```

- `erpc_signal_register()`用于监听系统的某个信号，当信号发生时调用传入的回调函数；
- `erpc_signal_unregister()`用于取消系统型号的监听。

下面示例程序中监听了用户自定义信号`SIGUSR1`：

```
#include <signal.h>
#include "erpc/erpc.h"
#include "simpost_component.hpp"

class componentSignal: public simpost::Component
{
public:
    explicit componentSignal() : Component("moduleSignal", "V0.1.0") {}
    virtual ~componentSignal() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    void signalHandler(erpc::erpc_signal_t handle, int signo, void *arg);
private:
    erpc::erpc_signal_t m_signal_ctx;
};

bool componentSignal::init(void)
{
    LOG_INFO("componentSignal init");
    m_signal_ctx = erpc::erpc_signal_register(SIGUSR1, std::bind(&componentSignal::signalHandler, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3), nullptr);
    return true;
}

void componentSignal::exit(void)
{
    LOG_INFO("componentSignal exit");
}

void componentSignal::signalHandler(erpc::erpc_signal_t handle, int signo, void *arg)
{
    LOG_INFO("recv system signal: {}", signo);
}

COMPONENT_REGISTER(componentSignal);
```

上面程序在初始化时，监听了用户自定义信号`SIGUSR1`，并在回调函数中打印了信号值，可以使用`kill -10 <pid>`命令，给程序发送该信号。运行效果如下：

```
$ ./bin/demoSignal 
[2024-04-19 09:41:01.431] [-I-] [simpost_application.cxx:54 log()] : ##############################################################################
[2024-04-19 09:41:01.431] [-I-] [simpost_application.cxx:55 log()] : demoSignal-V0.4.0.0
[2024-04-19 09:41:01.431] [-I-] [simpost_application.cxx:56 log()] : workspace: /home/konishi/simpostOS
[2024-04-19 09:41:01.431] [-I-] [simpost_application.cxx:58 log()] : code state: develop-09bbc78
[2024-04-19 09:41:01.431] [-I-] [componentSignal.cxx:20 init()] : componentSignal init
[2024-04-19 09:41:01.431] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-19 09:41:01.431] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-19 09:41:38.155] [-I-] [componentSignal.cxx:32 signalHandler()] : recv system signal: 10
[2024-04-19 09:41:48.446] [-I-] [componentSignal.cxx:32 signalHandler()] : recv system signal: 10
[2024-04-19 09:41:48.984] [-I-] [componentSignal.cxx:32 signalHandler()] : recv system signal: 10
[2024-04-19 09:41:49.450] [-I-] [componentSignal.cxx:32 signalHandler()] : recv system signal: 10
```
