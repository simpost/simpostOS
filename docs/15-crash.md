# 程序崩溃问题定位示例 #

本示例程序是基于前面组件式编程基础上演变而来。

## 一、创建主程序

在组件式编程的主程序中，调用`traceEnable()`使能崩溃日志生成功能，注意该函数需要在`app.coreInit()`函数之后调用：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. backtrace enable
    if(false == app.traceEnable())
        return -1;
    ///< 4. component init
    if(false == app.componentInit())
        return -1;
    ///< 5. application run
    return app.exec();
}
```

## 二、实现崩溃组件

下面组件在业务下城中访问非法地址，导致程序崩溃：

```
#include <thread>
#include "config.h"
#include "simpost_component.hpp"

class crashDemo: public simpost::Component
{
public:
    explicit crashDemo() : Component("crashDemo", SERVICE_VERSION_STR) {}
    virtual ~crashDemo() = default;
    bool init(void) override final;
    void exit(void) override final;
private:
    static void dump_routine(void);
};

void crashDemo::dump_routine(void)
{
    volatile int* a = (int*)(NULL);
    std::this_thread::sleep_for(std::chrono::microseconds(5));
    LOG_INFO("crash here");
    *a = 1;
}

bool crashDemo::init(void)
{
    LOG_INFO("crashDemo init");
    std::thread crash(dump_routine);
    crash.detach();
    return true;
}

void crashDemo::exit(void)
{
    LOG_INFO("crashDemo exit");
}

COMPONENT_REGISTER(crashDemo);
```

## 三、崩溃日志定位问题

构建生成可执行程序：

```
add_executable(demoCrash demoCrash.cxx demoAPP.cxx)
target_link_libraries(demoCrash PRIVATE application protobuf erpc yaml-cpp uv)
```

执行`demoCrash`程序，程序崩溃时栈回溯日志如下：

```
[2024-04-12 09:51:27.939] [-I-] [simpost_application.cxx:49 init()] : #####################################################################################
[2024-04-12 09:51:27.939] [-I-] [simpost_application.cxx:50 init()] : demoCrash version: V0.4.0.0
[2024-04-12 09:51:27.939] [-I-] [simpost_application.cxx:51 init()] : workspace: /home/konishi/simpostOS
[2024-04-12 09:51:27.939] [-I-] [simpost_application.cxx:53 init()] : code state branch: develop, commit: f7c50fa
[2024-04-12 09:51:27.940] [-I-] [erpc_profile.cxx:106 basicConfig()] : basicConfig: application
[2024-04-12 09:51:27.940] [-I-] [erpc_profile.cxx:107 basicConfig()] : listenPort: 17010, bufferSize: 1024
[2024-04-12 09:51:27.940] [-I-] [erpc_profile.cxx:108 basicConfig()] : source mask: 127.0.0.1/
[2024-04-12 09:51:27.940] [-I-] [erpc_profile.cxx:148 parseNetConfig()] : dependService:
[2024-04-12 09:51:27.940] [-I-] [erpc_profile.cxx:178 parseNetConfig()] :  - service(demoService), 127.0.0.1:17011
[2024-04-12 09:51:27.940] [-I-] [erpc_transmit.cxx:179 erpc_transmit_init()] : transmit buffer size: 1024
[2024-04-12 09:51:27.940] [-I-] [erpc_router.hpp:146 routine()] : message router is runing
[2024-04-12 09:51:27.940] [-I-] [erpc_threadpool.hpp:133 threadRoutine()] : ThreadPool: static routine runing
[2024-04-12 09:51:27.940] [-I-] [erpc_threadpool.hpp:133 threadRoutine()] : ThreadPool: static routine runing
[2024-04-12 09:51:27.940] [-I-] [demoCrash.cxx:26 init()] : crashDemo init
[2024-04-12 09:51:27.940] [-I-] [erpc_threadpool.hpp:133 threadRoutine()] : ThreadPool: static routine runing
[2024-04-12 09:51:27.940] [-I-] [demoCrash.cxx:20 dump_routine()] : crash here
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:171 backtrace_handler()] : Crash backstrace for signal: 11
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   0 deep: /home/konishi/simpostOS/lib/libapplication.so.0(+0x51f21) [0x7f978c294f21]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   1 deep: /home/konishi/simpostOS/lib/liberpc.so.0(+0x6b03b) [0x7f978bdd403b]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   2 deep: /home/konishi/simpostOS/lib/libuv.so.1(+0x1ae46) [0x7f978bb52e46]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   3 deep: /home/konishi/simpostOS/lib/libuv.so.1(+0x2413e) [0x7f978bb5c13e]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   4 deep: /home/konishi/simpostOS/lib/libuv.so.1(uv_run+0x16d) [0x7f978bb4932d]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   5 deep: /home/konishi/simpostOS/lib/liberpc.so.0(_ZN4erpc15erpc_looper_runEv+0xab) [0x7f978bdd45eb]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   6 deep: ./bin/demoCrash(+0x1ecd1) [0x55e623f9acd1]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   7 deep: /lib/x86_64-linux-gnu/libc.so.6(__libc_start_main+0xf3) [0x7f978be3a083]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:173 backtrace_handler()] :   8 deep: ./bin/demoCrash(+0x1ed6e) [0x55e623f9ad6e]
[2024-04-12 09:51:27.941] [-E-] [simpost_utils.cxx:175 backtrace_handler()] : error exited!
[2024-04-12 09:51:27.941] [-I-] [erpc_threadpool.hpp:151 threadRoutine()] : ThreadPool: static routine will exit.
[2024-04-12 09:51:27.941] [-I-] [erpc_threadpool.hpp:151 threadRoutine()] : ThreadPool: static routine will exit.
[2024-04-12 09:51:27.941] [-I-] [erpc_threadpool.hpp:151 threadRoutine()] : ThreadPool: static routine will exit.
[2024-04-12 09:51:27.971] [-I-] [erpc_router.hpp:162 routine()] : message router will exit
```

若想查看更加详细的信息，可以使用调试方法构建程序（`-g`）。
