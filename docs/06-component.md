# 组件示例程序 #

## 一、组件主程序源码

开发组件化应用程序，主程序在`helloworld`的基础上，去掉`helloworld_routine`业务线程，并在执行`app.exec()`函数之前，调用`app.componentInit()`即可，下面是完整的主程序源码：

```
#include "config.h"
#include "simpost_application.hpp"

int main(int argc, char *argv[])
{
    simpost::Application app(argc, argv, SERVICE_VERSION_STR);
    ///< 1. logger init
    if(false == app.logInit(simpost::LOG_TYPE_CONSOLE, , SERVICE_VERSION_DESCR, SERVICE_IMAGE_DESCR))
        return -1;
    ///< 2. core init
    if(false == app.coreInit())
        return -1;
    ///< 3. component init
    if(false == app.componentInit())
        return -1;
    ///< 4. application run
    return app.exec();
}
```

其中，`SERVICE_VERSION_STR`、`SERVICE_BRANCH_NAME`和`SERVICE_COMMIT_HASH`来源于`cmake`构建工具的自动化获取。

## 二、编写组件应用程序

组件应用程序编码流程如下：

1. 包含头文件：`simpost_component.hpp`;
2. 继承`simpost::Component`定义组件的子类；
3. 复写`init()`和`exit()`函数，分别在进程启动与退出时调用；
4. 使用`COMPONENT_REGISTER()`将组件子类添加到组件框架中。

注意：对于`init()`函数的返回值：若返回`false`，则程序会立即结束；若返回`true`，程序才进入事件驱动循环。

比如下面源码分别实现了两个组件应用：

- `componentA.cxx`

```
#include "simpost_component.hpp"

class componentA: public simpost::Component
{
public:
    explicit componentA() : Component("moduleA", "V0.1.0") {}
    virtual ~componentA() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool componentA::init(void)
{
    LOG_INFO("componentA init");
    return true;
}

void componentA::exit(void)
{
    LOG_INFO("componentA exit");
}

COMPONENT_REGISTER(componentA);
```

- `componentB.cxx`

```
#include "simpost_component.hpp"

class componentB: public simpost::Component
{
public:
    explicit componentB() : Component("moduleB", "V0.1.0") {}
    virtual ~componentB() = default;
    bool init(void) override final;
    void exit(void) override final;
};

bool componentB::init(void)
{
    LOG_INFO("componentB init");
    return true;
}

void componentB::exit(void)
{
    LOG_INFO("componentB exit");
}

COMPONENT_REGISTER(componentB);
```

## 三、构建组件应用程序

组件程序的构建，只需要将组件源码和主程序一同构建即可。比如上面的组件A与组件B可以有如下三种构建形式：

```
///< 将 componentA 构建到进程中
add_executable(componentA demoAPP.cxx componentA.cxx)
target_link_libraries(componentA PRIVATE application protobuf erpc yaml-cpp uv)

///< 将 componentB 构建到进程中
add_executable(componentB demoAPP.cxx componentB.cxx)
target_link_libraries(componentB PRIVATE application protobuf erpc yaml-cpp uv)

///< 将 componentA 和 componentB 都构建到同一个进程中
add_executable(componentAll demoAPP.cxx componentA.cxx componentB.cxx)
target_link_libraries(componentAll PRIVATE application protobuf erpc yaml-cpp uv)
```

## 四、运行组件应用程序

组件程序运行结果如下：

```
$ source setup.sh
$ ./bin/demoAPP
$ ./bin/componentA
[2024-04-16 13:24:22.939] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 13:24:22.939] [-I-] [simpost_application.cxx:54 log()] : componentA version: V0.4.0.0
[2024-04-16 13:24:22.939] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 13:24:22.939] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: cd8e4a7
[2024-04-16 13:24:22.939] [-I-] [componentA.cxx:14 init()] : componentA init
[2024-04-16 13:24:22.939] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:24:22.939] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing

$ ./bin/componentB
[2024-04-16 13:24:53.054] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 13:24:53.055] [-I-] [simpost_application.cxx:54 log()] : componentB version: V0.4.0.0
[2024-04-16 13:24:53.055] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 13:24:53.055] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: cd8e4a7
[2024-04-16 13:24:53.055] [-I-] [componentB.cxx:14 init()] : componentB init
[2024-04-16 13:24:53.055] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:24:53.055] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing

$ ./bin/componentAll
[2024-04-16 13:25:22.226] [-I-] [simpost_application.cxx:53 log()] : #####################################################################################
[2024-04-16 13:25:22.226] [-I-] [simpost_application.cxx:54 log()] : componentAll version: V0.4.0.0
[2024-04-16 13:25:22.226] [-I-] [simpost_application.cxx:55 log()] : workspace: /home/konishi/simpostOS
[2024-04-16 13:25:22.226] [-I-] [simpost_application.cxx:57 log()] : code state branch: develop, commit: cd8e4a7
[2024-04-16 13:25:22.227] [-I-] [componentB.cxx:14 init()] : componentB init
[2024-04-16 13:25:22.227] [-I-] [componentA.cxx:14 init()] : componentA init
[2024-04-16 13:25:22.227] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
[2024-04-16 13:25:22.227] [-I-] [erpc_threadpool.hpp:132 threadRoutine()] : ThreadPool: static routine runing
```